package comparators;

import java.util.Comparator;
import model.StateProtocol;

public class StateProtocolComparator implements Comparator<StateProtocol> {
	
	int direction = 1;
	
	public StateProtocolComparator(int direction) {
		if(direction!=1 && direction!=-1){
			direction = 1;
		}
		this.direction = direction;
	}
	
	@Override
	public int compare(StateProtocol sp1, StateProtocol sp2) {
		
		if (sp1 == null || sp2 == null) {
			System.out.println("Ne postoje datumi za poredjenje!");
			System.exit(1);
		}
		
		if ( sp1.getDate().before(sp2.getDate())) {
            return -1*direction;
        } else if (sp1.getDate().after(sp2.getDate())) {
            return 1*direction;
        } else {
            return 0;
        } 
		
	}

}
