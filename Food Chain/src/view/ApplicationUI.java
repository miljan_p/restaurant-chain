package view;

import controller.FoodChainStore;
import model.Customer;
import model.Deliverer;
import model.User;
import model.Worker;
import utility.Utility;

public class ApplicationUI {
	
	public static int DELIVERY = 150;
	public static int CREDIT_FOR_ORDER = 1;
	public static int LOWER_LIMIT_FOR_CREDIT = 1000;
	public static int MAX_CREDIT_PER_ORDER = 10;
	public static int DISCOUNT_PER_CREDIT = 3; //percentages

	public static void main(String[] args) {
		
		FoodChainStore.initialLoad(); //postavljanje referenci izmedju objekata
		
		System.out.println();
		System.out.println("Dobrodo�li kod Mrvice!");
		
		
		
		while (true) {
			System.out.println();
			System.out.println("Izaberite nacin logovanja na na� sistem.");
			System.out.println("1. Mu�terija \n2. Dostavljac\n3. Radnik\n0. Izlaz iz aplikacije");
			
			int uloga = Utility.choice(3);
			String username = Utility.ocitajTekst("Unesite Va�e korisnicko ime: ");
			String password = Utility.ocitajTekst("Unesite Va�u lozinku: ");
			switch (uloga) {
				
				case 1:
					User customer = FoodChainStore.login(username, password, new Customer());
					
					if (customer instanceof Customer) {
						Customer loggedCustomer = (Customer) customer;
						CustomerUI.customerMenu(loggedCustomer);
					} else {
						System.out.println("Pogre�no une�ena korisnicko ime ili lozinka.");
					}
					break;
				case 2:
					User deliverer = FoodChainStore.login(username, password, new Deliverer());
					
					if (deliverer instanceof Deliverer) {
						Deliverer loggedDeliverer = (Deliverer) deliverer;
						DelivererUI.delivererMenu(loggedDeliverer);
					} else {
						System.out.println("Pogre�no une�ena korisnicko ime ili lozinka.");
					}
					break;
				case 3:
					User worker = FoodChainStore.login(username, password, new Worker());
					
					if (worker instanceof Worker) {
						Worker loggedWorker = (Worker) worker;
						WorkerUI.workerMenu(loggedWorker);
					} else {
						System.out.println("Pogre�no une�ena korisnicko ime ili lozinka.");

					}	
					break;
				case 0:
					break;
					
			}
		}
		
		
		
		
		
	}

}
