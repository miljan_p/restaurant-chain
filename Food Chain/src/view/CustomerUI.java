package view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import controller.FoodChainStore;
import model.Bonus;
import model.Customer;
import model.Element;
import model.Item;
import model.Order;
import model.Restaurant;
import model.StateProtocol;
import model.Status;
import model.User;
import utility.Utility;


public class CustomerUI {
	
	
	public static void customerMenu(Customer loggedCustomer){
		
		System.out.println();
		System.out.println("Dobrodo�li: " + loggedCustomer.getUsername());
		System.out.println();
		
		while (true) {
			
			System.out.println("Izaberite zeljenu opciju.");
			System.out.println("1. Napravi novu prodzbinu\n2. Izmeni porucene porudzbine\n3. Otkazi porucene porudzbine\n4. Povratak u prethodni meni\n0. Izalazak iz aplikacije");
		
			int izbor = Utility.choice(4);
			
			switch (izbor) {
			
				case 1:
					makeNewOrder(loggedCustomer);
					break;
				case 2:
					editOrderedOrders(loggedCustomer);
					break;
				case 3:
					cancelOrderedOrders(loggedCustomer);
					break;
				case 4:
					return;
				case 0:
					System.exit(1);
					
			}
	
		}
			
	} //od customerMeni metode
	
	public static void cancelOrderedOrders(Customer loggedCustomer) {
		System.out.println("Izaberite id porudzbine koju zelite da otkazete");
		
		ArrayList<Order> orders = findOrders(loggedCustomer);
		if (orders.size() == 0) {
			System.out.println("Ne postoje porudzbine u sistemu sa Va�im korisnickim imenom ili su sve Va�e porudzbine u realizaciji");
			return;
		}
		
		displayOrdersForUser(orders);
		
		int indexOrder = Utility.choiceForElement(orders.size()-1, "Izaberite redni broj porudzbine koju zelite da otkazete: ");
		
		Order canceledOrder = orders.get(indexOrder);
		
		StateProtocol cancelProtocol = new StateProtocol();
		cancelProtocol.setId(FoodChainStore.generateId(FoodChainStore.protocols));
		cancelProtocol.setOrder(canceledOrder.getId());
		cancelProtocol.setDate(new Date());
		cancelProtocol.setStatus(Status.Canceled);
		
		FoodChainStore.protocols.put(cancelProtocol.getId(), cancelProtocol);
		canceledOrder.getStates().add(cancelProtocol);
		
		try {
			FoodChainStore.dodaj(cancelProtocol);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	

	public static void makeNewOrder(Customer loggedCustomer)  {
		
		var newOrder = new Order();
		
		//1.
		newOrder.setId(FoodChainStore.generateId(FoodChainStore.orders));
		
		//2.
		newOrder.setElements(addNewElements(newOrder.getId()));
		if (newOrder.getElements() == null) return;
		
		//3.
		StateProtocol newStateProtocol = new StateProtocol(FoodChainStore.generateId(FoodChainStore.protocols), newOrder.getId(), Status.Ordered, new Date());
		newOrder.setStates( new ArrayList<StateProtocol>(List.of(newStateProtocol)));
		
		
		//4.
		newOrder.setUser(loggedCustomer);
		
		//5.
		newOrder.setRemarks(Utility.ocitajTekst("Napomena: "));
		
		//6.
		newOrder.setAktivna(true);
		
		//7.
		newOrder.setRestaurantPickup(findRestaurnatPickup(newOrder.getElements()));

		//8.
		useBonus(newOrder, loggedCustomer);
		
		
		//9.
		newOrder.setDelivery(ApplicationUI.DELIVERY);
		
		//
		FoodChainStore.protocols.put(newStateProtocol.getId(), newStateProtocol);
		FoodChainStore.orders.put(newOrder.getId(), newOrder);
		
		try {
			FoodChainStore.dodaj(newOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		public static ArrayList<Element> addNewElements(int newIdOrder){
			
			ArrayList<Element> elements = new ArrayList<Element>();
			
			System.out.println(showAllRestaurantsOffer());
			System.out.println();
			
			
			while(true) {
				System.out.println();
				System.out.println("1. Dodaj artikle\n2. Ukloni artikal\n3. Izmeni kolicinu artikla\n4. Potvrdi porudzbinu\n0. Poni�ti porudzbinu");
			
				int izbor = Utility.choice(4);
				
				switch (izbor) {
				
					case 1: 
						addElements(elements, newIdOrder);
						showOrder(elements);
						break;
					case 2:
						removeElements(elements);
						showOrder(elements);
						break;
					case 3:
						editAmounts(elements);
						showOrder(elements);
						break;
					case 4:
						if (elements.size() == 0) return null;					
						return elements;
					case 0:
						return null;
				}
			}
			
		}
		
		
		
		public static void addElements(ArrayList<Element> elements, int newIdOrder) {
			
			do {
				showOrder(elements);
				
				Element newElement = new Element();
				
				newElement.setId(FoodChainStore.generateId(FoodChainStore.elements));
				
				var idItem = Utility.ocitajId(FoodChainStore.items, "Izaberite id artikla koji zelite u porudzbini: " );
				
				newElement.setItem((Item)FoodChainStore.items.get(idItem));
				
				newElement.setAmount(Utility.ocitajCeoBroj("Unesite kolicinu: "));
				
				newElement.setOrder(newIdOrder);
				
				newElement.setBonusElement(false);
				
				elements.add(newElement); //dodajemo ga u listu elemenata
				
				FoodChainStore.elements.put(newElement.getId(), newElement); //dodajemo ga i u global mapu
				
				
			} while (Utility.ocitajOdlukuOPotvrdi("da dodate novi artikal") == 'Y');
		}
			

		public static void removeElements(ArrayList<Element> elements) {

			System.out.println();
			do {
				if (elements.size() == 0) {
					System.out.println("Lista porudzbine je prazna."); 
					return;
				}
				
				showElements(elements);
				
				int removeIndex = Utility.choiceForElement(elements.size() - 1, "Izaberite redni broj artikla koji zelite da ukolnite: ");
				FoodChainStore.elements.remove(elements.get(removeIndex).getId());
				elements.remove(removeIndex);
			
			} while (Utility.ocitajOdlukuOPotvrdi("da uklonite jo� artikala") == 'Y');
		}
			
		public static void showElements(ArrayList<Element> elements) {
			int index = 1;
			for (Element e : elements) {
				System.out.println(index++ + "." + " " + e + "\n");
			}
		}
		
	
	
	public static String showAllRestaurantsOffer() {
		
		BiConsumer<StringBuilder, String> append = StringBuilder::append;
		String offer =  FoodChainStore.restaurants.entrySet().stream()
				.map(i -> ((Restaurant) i.getValue()))
				.map(Restaurant::showOffer)
				.collect(StringBuilder::new, append, StringBuilder::append)
				.toString();
		return offer;
				
	}
	
	public static Restaurant findRestaurnatPickup(ArrayList<Element> elements) {
		
		//ints - lista sa id-evima restorana odakle su aritkli
		ArrayList<Integer> idRestaurants = (ArrayList<Integer>) elements.stream()
				.map(i-> i.getItem().getRestaurant())
				.collect(Collectors.toList());
		
		
		Integer max = null; //element sa najvecim brojem ponavljanja
	 	HashMap<Integer,Integer> mapa = new HashMap<Integer,Integer>();
	  	
	    for(int i : idRestaurants){
	        if(!mapa.containsKey(i)) {
	        	mapa.put(i,1);
	        } else {
	        	mapa.put(i, mapa.get(i)+1);
	        }
	        
	        if(max == null) max = i;
	    	else if(mapa.get(i) >= mapa.get(max)) max = i;
	    }
		
		return (Restaurant) FoodChainStore.restaurants.get(max);
	}
	
	public static void showOrder(ArrayList<Element> elements) {
		if (elements.size() == 0) {
			System.out.println("Va�a porudzbina je prazna.");
			return;
		}
		StringBuilder sb = new StringBuilder();
		System.out.println("Trenutno stanje Va�e porudzbine je:");
		
		int total = 0;
		for (Element e : elements) {
			double cena = e.getItem().getPrice()*e.getAmount();
			total += cena;
			sb.append("Artikal: " + e.getItem().getDescription() + " " + "kolicina: " + e.getAmount() + " " + "cena: " + cena + "\n");	
		}
		total+=ApplicationUI.DELIVERY;
		System.out.print(sb.toString() + "Dostava: " + ApplicationUI.DELIVERY + "\n" + "UKUPNO: " + total);
		System.out.println();
		System.out.println();
	}
	
	public static void editAmounts(ArrayList<Element> elements) {
		
		do {
			System.out.println("Izaberite redni broj artikla ciji kolicinu zelite da promenite.");
			showElements(elements);
			int indexElementa = Utility.choiceForElement(elements.size() - 1 , "Izaberite redni broj artikla ciji kolicinu zelite da promenite.");
			Element element = elements.get(indexElementa);
			System.out.println("Trenutna vrednost kolicine izabranog artikla: " + element.getAmount());
			int newAmount = Utility.ocitajCeoBroj("Unesite novu kolicinu za izabrani artikal: ");
			element.setAmount(newAmount);	
		} while (Utility.ocitajOdlukuOPotvrdi("izmenite kolicine drugih artikala") == 'Y');
		
		return;
	}
	
	public static void useBonus(Order newOrder, Customer loggedCustomer) {
		
		if (Utility.ocitajOdlukuOPotvrdi("iskoristite bonus na ovu pordzbinu") == 'Y') {
			
			if (loggedCustomer.getCredit() == 0) {
				System.out.println("Nemate bounus kredita na Va�em nalogu.");
				return;
			}
			
			System.out.println("Stanje Va�eg kredita je: " + loggedCustomer.getCredit());
			System.out.println("Napomena: Makismalan dozvoljeni iznos kredita koji mozete da upotrebite iznosi " + ApplicationUI.MAX_CREDIT_PER_ORDER + " kredita.");
			
			
			while (true) {
				int creditForOrder = Utility.ocitajSentinelBroj("Unesite kolicinu kredita za ovu pordzbinu: ", ApplicationUI.MAX_CREDIT_PER_ORDER, loggedCustomer.getCredit());
				
				System.out.println("Cena porudzbine BEZ POPUSTA: " + newOrder.getTotal());
				//double discountPrice = newOrder.getTotal() - newOrder.getTotal() * ((creditForOrder * ApplicationUI.DISCOUNT_PER_CREDIT)/100);
				double discountPrice = newOrder.getCurrentDiscountPrice(creditForOrder);
				System.out.println("Cena porudzbine SA POPUSTOM: " + discountPrice);
				System.out.println("Dostava: " + ApplicationUI.DELIVERY);
				System.out.println("UKUPNO: " + (discountPrice+ApplicationUI.DELIVERY));
				
				if (Utility.ocitajOdlukuOPotvrdi("iskoristite navedeni bonus") == 'Y'){
					Bonus newBonus = new Bonus(FoodChainStore.generateId(FoodChainStore.bonuses) 
									,newOrder.getId(), creditForOrder 
									,newOrder.getTotal() - newOrder.getCurrentDiscountPrice(creditForOrder)
									,ApplicationUI.LOWER_LIMIT_FOR_CREDIT
									,ApplicationUI.CREDIT_FOR_ORDER
									,ApplicationUI.MAX_CREDIT_PER_ORDER
									,ApplicationUI.DISCOUNT_PER_CREDIT
									);
					loggedCustomer.setCredit(loggedCustomer.getCredit()-creditForOrder);
					FoodChainStore.bonuses.put(newBonus.getId(), newBonus);
					newOrder.setBonus(newBonus);
					System.out.println("Uspe�no ste iskoristili bonus na porudzbinu.");
					return;

				}
				
				if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju unosa bonus kredita i da potvrdite porudzbinu") == 'Y') {
					break;
				}
			
			}
		
			
			
		} else {
			if (newOrder.getTotal() > ApplicationUI.LOWER_LIMIT_FOR_CREDIT) loggedCustomer.setCredit(loggedCustomer.getCredit() + ApplicationUI.CREDIT_FOR_ORDER);
			
			
		}
		
		return;
	}
	
	public static void editOrderedOrders(Customer loggedCustomer) {
		
		//System.out.println("Napomena: dozvoljeno je modifikovati one porudzbine koje nisu preuzete za realizaciju.");
		System.out.println();
		System.out.println("Izaberite id porudzbine koju zelite da modifikujete");
		
		ArrayList<Order> orders = findOrders(loggedCustomer);
		if (orders.size() == 0) {
			System.out.println("Ne postoje porudzbine u sistemu sa Va�im korisnickim imenom ili su sve Va�e porudzbine u realizaciji");
			return;
		}
		
		
		while (true) {
			System.out.println("1. Izmenite porudzbinu\n2. Otkazite porudzbinu\n0. Povratak na prethodni meni");
			int izbor = Utility.choice(2);
			
			switch (izbor) {
				case 1: 
					//izmeni porudzbinu
					modifyOrder(loggedCustomer, orders);
					break;
				case 2:
					//cancel porudzbinu
					break;
				case 0:
					//povratak na prethodni meni
					return;
			}
			
		}
		
	}
	
	public static void modifyOrder(Customer loggedCustomer, ArrayList<Order> orders) {
//		displayOrdersForUser(orders);
//		System.out.println();
//		int order = Utility.choiceForElement(orders.size()-1, "Izaberite redni broj porudzbine koju zelite da modifikujete: ");
//		
//		Order orderToModifiy = orders.get(order);
		
		while (true) {
			System.out.println();
			System.out.println();

			displayOrdersForUser(orders);
			System.out.println();
			System.out.println("1. Ukloni elemente\n2. Dodaj artikal\n3. Izmeni kolicine\n4. Aktiviraj bonus\n5. Izmeni bonus\n6. Deaktiviraj bonus kredit\n0. Povratak na prethodni meni");

			int order = Utility.choiceForElement(orders.size()-1, "Izaberite redni broj porudzbine koju zelite da modifikujete: ");
			
			Order orderToModifiy = orders.get(order);
//			System.out.println("1. Ukloni elemente\n2. Dodaj artikal\n3. Izmeni kolicine\n4. Aktiviraj bonus\n5. Izmeni bonus\n6. Deaktiviraj bonus kredit\n0. Povratak na prethodni meni");
			int choice = Utility.choice(6);
			
			switch(choice) {
			
				case 1:
					removeElementsForOrder(orderToModifiy.getElements());
					if (orderToModifiy.getBonus() != null) {
						recalculateBonus(orderToModifiy);
					} else {
						if (orderToModifiy.getTotal() > ApplicationUI.LOWER_LIMIT_FOR_CREDIT) {
							loggedCustomer.setCredit(loggedCustomer.getCredit()+ApplicationUI.CREDIT_FOR_ORDER);
						}
					}
					orderToModifiy.setRestaurantPickup(findRestaurnatPickup(orderToModifiy.getElements()));
					
					
					
					try {
						FoodChainStore.izmeni(orderToModifiy);
						FoodChainStore.izmeni(loggedCustomer);
						if (orderToModifiy.getBonus() != null) {
							FoodChainStore.izmeni(orderToModifiy.getBonus());
						}
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					
					showEditedOrder(orderToModifiy);
					break;
				case 2:
					//dodaj artikal sa kolicinom
					addElements(orderToModifiy);
					break;
				case 3:
					editAmountsForOrder(orderToModifiy);

					showEditedOrder(orderToModifiy);
					break;
				case 4:
					activateBonus(orderToModifiy, loggedCustomer);
					
					
					showEditedOrder(orderToModifiy);
					break;
				case 5:
					//izmeni bonus
					//ova metoda prima por samo koje imaju bonus...
					editBonus(orderToModifiy, loggedCustomer);

						
					
					break;
				case 6:
					//ova metoda prima ordere samo koje imaju bonus
					deactivateBonus(orderToModifiy, loggedCustomer);
					
					break;
				case 0:
					return;
			}
		}
		
	}
	
	public static void addElements(Order orderToModifiy) {
		while (true) {
			System.out.println(showAllRestaurantsOffer());
			
			Element newElement = new Element();
			
			newElement.setId(FoodChainStore.generateId(FoodChainStore.elements));
			
			var idItem = Utility.ocitajId(FoodChainStore.items, "Izaberite id artikla koji zelite da dodate u porudzbinu: " );
			
			newElement.setItem((Item)FoodChainStore.items.get(idItem));
			
			newElement.setAmount(Utility.ocitajCeoBroj("Unesite kolicinu: "));
			
			newElement.setOrder(orderToModifiy.getId());
			
			newElement.setBonusElement(false);
			
			orderToModifiy.getElements().add(newElement); //dodajemo ga u listu elemenata
			
			if (orderToModifiy.getBonus() != null) {
				recalculateBonus(orderToModifiy);
			}
			
			
			showEditedOrder(orderToModifiy);
			
			if (Utility.ocitajOdlukuOPotvrdi("da dodate novi artikal u porudzbinu") == 'Y') {
				FoodChainStore.elements.put(newElement.getId(), newElement); //dodajemo ga i u global mapu
				orderToModifiy.setRestaurantPickup(findRestaurnatPickup(orderToModifiy.getElements()));
				try {
					FoodChainStore.dodaj(newElement);
					FoodChainStore.izmeni(orderToModifiy);
					if (orderToModifiy.getBonus() != null) {
						FoodChainStore.izmeni(orderToModifiy.getBonus());
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
			} else {
				orderToModifiy.getElements().remove(orderToModifiy.getElements().size()-1);
				
			}
			if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju uno�enja novih artikal i potvrdite porudzbinu") == 'Y') {
				return;
			}
			
			
			
		//(
		} //od while
			
	}
	
	public static void deactivateBonus(Order orderToModifiy,Customer loggedCustomer) {
		//ova metoda prima ordere samo koje imaju bonus
		if (orderToModifiy.getBonus() == null) {
			System.out.println("Morate aktvirati bonus za ovu porudzbinu");
			return;
		}
		
		//int oldStateCredit = loggedCustomer.getCredit()+orderToModifiy.getBonus().getCredits();
		//int oldCreditOrder = loggedCustomer.getCredit();
		Bonus oldBonus = orderToModifiy.getBonus();
		orderToModifiy.setBonus(null);
		
		showEditedOrder(orderToModifiy);
		
		if (Utility.ocitajOdlukuOPotvrdi("deaktivirati postojeci bonus na ovoj porudzbini") == 'Y') {
			//loggedCustomer.setCredit(loggedCustomer.getCredit()+orderToModifiy.getBonus().getCredits());
			try {
				FoodChainStore.obrisi(orderToModifiy.getBonus());
				FoodChainStore.izmeni(loggedCustomer);
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			orderToModifiy.setBonus(oldBonus);
		}
		
		if (Utility.ocitajOdlukuOPotvrdi("da prekinete akciju deaktiviranja bonusa i potvrdite porudzbinu") == 'Y') {
			return;
		}
		
	}
	
	public static void editBonus(Order orderToModifiy,Customer loggedCustomer) {
		//ova metoda prima porudzbine samo koje imaju bonus...
		if (orderToModifiy.getBonus() == null) {
			System.out.println("Morate aktivirati bonus za ovu porudzbinu.");
			return;
		}
		
		while (true) {
			int oldCredit = loggedCustomer.getCredit();
			int oldCreditState = loggedCustomer.getCredit() + orderToModifiy.getBonus().getCredits();
			System.out.println("Stanje Va�eg kredita: " + oldCreditState);
			System.out.println("Stara vrednost kredita iskori�cena za ovu porudzbinu: " + orderToModifiy.getBonus().getCredits());
			System.out.println("Maksimalan dozvoljeni iznos kredita koji mozete iskoristiti: " + orderToModifiy.getBonus().getMaxCreditPerOrder());
			
			int newCredit = Utility.ocitajSentinelBroj("Unesite novi kredit za porudzbinu: ", oldCreditState, orderToModifiy.getBonus().getMaxCreditPerOrder());
			orderToModifiy.getBonus().setCredits(newCredit);
			recalculateBonus(orderToModifiy);
			
			showEditedOrder(orderToModifiy);
			
			if (Utility.ocitajOdlukuOPotvrdi("postavite novu vrednost bonus kredita") == 'Y') {
				try {
					FoodChainStore.izmeni(orderToModifiy.getBonus());
					loggedCustomer.setCredit(oldCreditState-newCredit);
					FoodChainStore.izmeni(loggedCustomer);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				orderToModifiy.getBonus().setCredits(oldCredit);
				recalculateBonus(orderToModifiy);
			}
			
			
			if (Utility.ocitajOdlukuOPotvrdi("da prekinete akciju promene kredita i potvrdite porudzbinu") == 'Y') {
				return;
			}			
		}
	}
	
	public static void activateBonus(Order orderToModifiy, Customer loggedCustomer) {
		//metoda dozvoljava samo aktiviranje bonusa ukoliko bonus vec nije iskoriscen za ovu porudzbinu
		if (orderToModifiy.getBonus() != null) {
			System.out.println("Ova porudzbina vec ima iskoriscen bonus. Izaberite opciju: -Izmeni bonus- , za modifikovanje bonus kredita");
			return;
		}
		
		if (loggedCustomer.getCredit() == 0) {
			System.out.println("Nemate bonus kredita na Va�em nalogu, ne mozete da iskoristite kredit.");
			return;
		}
		
//		//ovde sam povecao kredit customera za onoliko koliko je prvobitno bilo kredita iskorisceno za ovaj bonus
//		int bonusForOrder = orderToModifiy.getBonus().getCredits();
//		loggedCustomer.setCredit(loggedCustomer.getCredit()+bonusForOrder);
		
		System.out.println("Stanje Va�eg kredita je: " + loggedCustomer.getCredit());
		System.out.println("Napomena: Makismalan dozvoljeni iznos kredita koji mozete da upotrebite iznosi " + ApplicationUI.MAX_CREDIT_PER_ORDER + " kredita.");
		

		while (true) {
			
			//showEditedOrder(orderToModifiy);
			
			int creditForOrder = Utility.ocitajSentinelBroj("Unesite kolicinu kredita za ovu pordzbinu: ", ApplicationUI.MAX_CREDIT_PER_ORDER, loggedCustomer.getCredit());
			
			System.out.println("Cena porudzbine BEZ POPUSTA: " + orderToModifiy.getTotal());
			//double discountPrice = newOrder.getTotal() - newOrder.getTotal() * ((creditForOrder * ApplicationUI.DISCOUNT_PER_CREDIT)/100);
			double discountPrice = orderToModifiy.getCurrentDiscountPrice(creditForOrder);
			System.out.println("Cena porudzbine SA POPUSTOM: " + discountPrice);
			System.out.println("Dostava: " + orderToModifiy.getDelivery());
			System.out.println("Cena porudzbine SA POPUSTOM + DOSTAVA: " + (discountPrice+orderToModifiy.getDelivery()));
			
			if (Utility.ocitajOdlukuOPotvrdi("iskoristite navedeni bonus") == 'Y'){
				Bonus newBonus = new Bonus(FoodChainStore.generateId(FoodChainStore.bonuses) 
								,orderToModifiy.getId(), creditForOrder 
								,orderToModifiy.getTotal() - orderToModifiy.getCurrentDiscountPrice(creditForOrder)
								,ApplicationUI.LOWER_LIMIT_FOR_CREDIT
								,ApplicationUI.CREDIT_FOR_ORDER
								,ApplicationUI.MAX_CREDIT_PER_ORDER
								,ApplicationUI.DISCOUNT_PER_CREDIT
								);
				loggedCustomer.setCredit(loggedCustomer.getCredit()-creditForOrder);
				FoodChainStore.bonuses.put(newBonus.getId(), newBonus);
				orderToModifiy.setBonus(newBonus);
				System.out.println("Uspe�no ste iskoristili bonus na porudzbinu.");
				
				try {
					FoodChainStore.dodaj(newBonus);
					FoodChainStore.izmeni(loggedCustomer);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				return;

			}
			
			if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju unosa bonus kredita i da potvrdite porudzbinu") == 'Y') {
				break;
			}
		
		}
		
		
		
		
	}
	
	public static void editAmountsForOrder(Order orderToModifiy) {
		
		while (true){
			System.out.println("Izaberite redni broj artikla ciji kolicinu zelite da promenite.");
			showElements(orderToModifiy.getElements());
			int indexElementa = Utility.choiceForElement(orderToModifiy.getElements().size() - 1 , "Izaberite redni broj artikla ciji kolicinu zelite da promenite.");
			
	
			Element element = orderToModifiy.getElements().get(indexElementa);
			System.out.println("Trenutna vrednost kolicine izabranog artikla: " + element.getAmount());
			int oldAmount = element.getAmount();
			int newAmount = Utility.ocitajCeoBroj("Unesite novu kolicinu za izabrani artikal: ");
			element.setAmount(newAmount);	
			
			showEditedOrder(orderToModifiy);
			
			if(Utility.ocitajOdlukuOPotvrdi("postavite izabranu kolicinu za odabrani artikal") == 'Y') {
				try {
					FoodChainStore.izmeni(element);
					if (orderToModifiy.getBonus() != null) {
						recalculateBonus(orderToModifiy);
						FoodChainStore.izmeni(orderToModifiy.getBonus());
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				element.setAmount(oldAmount);
			}
			
			if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju promene kolicne i potvrdite porudzbinu") == 'Y') {
				return;
			}
			
		} 
		
	}
	
	public static void removeElementsForOrder(ArrayList<Element> elements) {
		System.out.println();
		do {
			if (elements.size() == 0) {
				System.out.println("Lista porudzbine je prazna."); 
				return;
			}
			
			showElements(elements);
			
			int removeIndex = Utility.choiceForElement(elements.size()-1, "Izaberite redni broj artikla koji zelite da ukolnite: ");
			
			
			//iz fajla elements.txt brisemo objekat
			try {
				FoodChainStore.obrisi((Element) elements.get(removeIndex));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//brisanje iz global mape
			FoodChainStore.elements.remove(elements.get(removeIndex).getId());
			//brisanje iz liste elemenata objekta porudzbine
			elements.remove(removeIndex);
			
		
		} while (Utility.ocitajOdlukuOPotvrdi("da uklonite jo� artikala") == 'Y');
	}
	
	public static void recalculateBonus(Order orderToModifiy) {
		Bonus bonusForOrder = orderToModifiy.getBonus();
		double newDeduction = orderToModifiy.getTotal() - orderToModifiy.getDiscountPrice();
		
		bonusForOrder.setDeduction(newDeduction);
		
		
	}
	
	
	
	public static void showEditedOrder(Order orderToModifiy) {
		displayOrdersForUser(new ArrayList<Order>(List.of(orderToModifiy)));
	}
	
	
	public static void displayOrdersForUser(ArrayList<Order> orders) {
		int index = 1;
		for (Order o : orders) {
			System.out.println();
			StringBuilder sb = new StringBuilder();
			System.out.println(index++ + ".  " + o.toStringCustomerOrder());
			//double total = 0;
			for (Element e : o.getElements()) {
				if (!e.isBonusElement()) {
					double cena = e.getItem().getPrice()*e.getAmount();
					//total += cena;
					sb.append("\tArtikal: " + e.getItem().getDescription() + " " + "kolicina: " + e.getAmount() + " " + "cena: " + cena + "\n");	
				} else {
					//ako jeste bonus element
					sb.append("\tArtikal: " + e.getItem().getDescription() + " " + "kolicina: " + e.getAmount() + " " + "cena: " + "BONUS ARTIKAL" + "\n");	

				}
			}
			
			if (o.getBonus() != null) {
				sb.append("\tBonus: iskoriscen" + " kredit: " + o.getBonus().getCredits() + " " + "umanjenje: " + o.getBonus().getDeduction()+"\n");
				sb.append("\tDostava: " + o.getDelivery()+"\n");
				System.out.println(sb.toString());
				System.out.println("\tCena sa POPUSTOM: " + " " + o.getDiscountPrice());
				System.out.println("\tCena sa POPUSTOM + DOSTAVA: " + (o.getDiscountPrice()+o.getDelivery()));
			} else {
				//nije iskoriscen kredit
				sb.append("\tBonus: nije iskoriscen"+"\n");
				sb.append("\tDostava: " + o.getDelivery()+"\n");
				sb.append("\tTotal: " + (o.getTotal()+o.getDelivery())+"\n");
				System.out.println(sb.toString());
			}
			//DOSTAVLJAAC
			
			sb = null;
		}
		System.out.println();
	}
	
	public static ArrayList<Order> findOrders(User loggedCustomer){
		
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getUser().getId() == loggedCustomer.getId()  &&  
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Ordered)
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
			
	}

} //od klase
