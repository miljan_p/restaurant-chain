package view;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import comparators.StateProtocolComparator;
import controller.FoodChainStore;
import model.Deliverer;
import model.Order;
import model.StateProtocol;
import model.Status;
import utility.Utility;

public class DelivererUI {
	
	
	public static void delivererMenu(Deliverer loggedDeliverer){
		
		System.out.println();
		System.out.println("Dobrodo�li: " + loggedDeliverer.getUsername());
		System.out.println();
		
		while (true) {
			
			System.out.println("Izaberite zeljenu opciju.");
			System.out.println("1. Prihvati zavr�enu prodzbinu\n2. Oznaci porudzbinu kao dostavljenu\n0. Izlaz iz aplikacije");
		
			int izbor = Utility.choice(1);
			ArrayList<Order> sortedOrders = null;
			switch (izbor) {
			
				case 1:
					sortedOrders = findAndSort();
					if(sortedOrders == null) {
						System.out.println("Ne postoje porudzbine na cekanju.");
						return;
					}
					acceptCompletedOrder(loggedDeliverer, sortedOrders);
					break;
				case 2:
					//
					sortedOrders = findAndSortDeliveringOrders(loggedDeliverer);
					if(sortedOrders == null) {
						System.out.println("Ne postoje porudzbine koje cekaju potvrdu dostave.");
						return;
					}
					markAsDelivered(sortedOrders);
					break;
				case 0:
					System.exit(1);
			}
		}//od while	
		
	}//od metode
	
	public static void markAsDelivered(ArrayList<Order> sortedOrders) {
		
		CustomerUI.displayOrdersForUser(sortedOrders);
		
		int indexOrder = Utility.choiceForElement(sortedOrders.size()-1, "Unesite redni broj porudzbine ciju dostavu zelite da potvrdite: ");
		Order delivererdOrder = sortedOrders.get(indexOrder);
		
		
		StateProtocol deliveredProtocol = new StateProtocol();
		deliveredProtocol.setId(FoodChainStore.generateId(FoodChainStore.protocols));
		deliveredProtocol.setOrder(delivererdOrder.getId());
		deliveredProtocol.setDate(new Date());
		deliveredProtocol.setStatus(Status.Delivered);
		
		FoodChainStore.protocols.put(deliveredProtocol.getId(), deliveredProtocol);
		
		try {
			FoodChainStore.dodaj(deliveredProtocol);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no dostavljena porudzbina!");
	}
	
	public static ArrayList<Order> findAndSortDeliveringOrders(Deliverer loggedDeliverer){
		
		ArrayList<Order> unsortedOrders = findDeliveringOrders(loggedDeliverer);
		if (unsortedOrders.size() == 0) {
			System.out.println("Ne postoje porudzbine na cekanju");
			return null;
		}
		System.out.println("Izaberite redosled prikaza preuzetih porudzbina.");
		System.out.println("1. Od najstarije ka najnovijoj\n2. Od najnovije ka najstarijoj");
		ArrayList<Order> sortedOrders = null;
		
		int izbor = Utility.choice(2);
		
		switch (izbor) {
			
			
			case 1:
					
				sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			case 2:
				
				 sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(-1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			default:
				System.out.println("Unesi sorter!");
				
		}
		return sortedOrders;
		
	}
	
	private static  ArrayList<Order> findDeliveringOrders(Deliverer loggedDeliverer){
		
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Delivering && ((Order) i.getValue()).getDeliverer().getId() == loggedDeliverer.getId() )
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
		
		
	}
	
	
	public static ArrayList<Order> findAndSort(){
		ArrayList<Order> unsortedOrders = findOrders();
		if (unsortedOrders.size() == 0) {
			System.out.println("Ne postoje porudzbine na cekanju");
			return null;
		}
		System.out.println("Izaberite redosled prikaza gotovih porudzbina.");
		System.out.println("1. Od najstarije ka najnovijoj\n2. Od najnovije ka najstarijoj");
		ArrayList<Order> sortedOrders = null;
		
		int izbor = Utility.choice(2);
		
		switch (izbor) {
			
			
			case 1:
					
				sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			case 2:
				 sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(-1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			default:
				System.out.println("Unesi sorter!");
				
		}
		return sortedOrders;
	}
	
	private static ArrayList<Order> findOrders() {
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Processed && ((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getDate().before(new Date()) )
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
		
			
	}
	
	
	public static void acceptCompletedOrder(Deliverer loggedDeliverer, ArrayList<Order> sortedOrders) {
		
		//prikazi 
		//izaberi
		//promeni state protocol -> interakcija sa fajlom
		
		CustomerUI.displayOrdersForUser(sortedOrders);
		
		int indexOrder = Utility.choiceForElement(sortedOrders.size()-1, "Unesite redni broj porudzbine: ");
		Order deliveringOrder = sortedOrders.get(indexOrder);
		deliveringOrder.setDeliverer(loggedDeliverer);
		
		StateProtocol deliveringProtocol = new StateProtocol();
		deliveringProtocol.setId(FoodChainStore.generateId(FoodChainStore.protocols));
		deliveringProtocol.setOrder(deliveringOrder.getId());
		deliveringProtocol.setDate(new Date());
		deliveringProtocol.setStatus(Status.Delivering);
		
		FoodChainStore.protocols.put(deliveringProtocol.getId(), deliveringProtocol);
		

		
		
		try {
			FoodChainStore.izmeni(deliveringOrder);
			FoodChainStore.dodaj(deliveringProtocol);
			//FoodChainStore.dodaj(deliveredProtocol);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	
	
//	private static Date inputDeliveredDate(Date deliveringDate) {
//		while (true) {
//			Date deliveredDate = Utility.unesiDatum("Unesite ocekivano vreme dostavljanja prihvacene porudzbine [dd-mm-yyyy hh:mm]: ");
//			if (deliveredDate.after(deliveringDate) && deliveredDate.after(new Date())) return deliveredDate;
//			else System.out.println("Vreme dostave mora biti nakon vremena prihvatanja porudzbine i nakon sada�njeg trenutka.");
//		}
//	}
	
	
	
	
} //od klase
