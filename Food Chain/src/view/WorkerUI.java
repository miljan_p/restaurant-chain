package view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import comparators.StateProtocolComparator;
import controller.FoodChainStore;
import model.Customer;
import model.Deliverer;
import model.Element;
import model.Item;
import model.Order;
import model.Restaurant;
import model.Role;
import model.StateProtocol;
import model.Status;
import model.Worker;
import utility.Utility;

public class WorkerUI {
	
public static void workerMenu(Worker loggedWorker){
		
		System.out.println();
		System.out.println("Dobrodo�li: " + loggedWorker.getUsername());
		System.out.println();
		
		while (true) {
			
			System.out.println("Izaberite zeljenu opciju.");
			
			System.out.println("1. Prihvati porucene porudzbine\n2. Dodaj novog klijenta\n3. Dodaj novog zaposlenog\n4. Dodaj ovlascena zaposlenom\n5. Povratak na prethodni meni\n6. Izve�taj\n0. Izlazak iz aplikacije");
			
			ArrayList<Order> sortedOrders = findAndSort();
			
			int izbor = Utility.choice(6);
			
			switch (izbor) {
				
			
				case 1: 					
					acceptOrders(loggedWorker, sortedOrders);
					break;
				case 2:					
					addNewCustomer(loggedWorker);
					break;
				case 3:
					addNewWorker(loggedWorker);
					break;
				case 4:
					authorize(loggedWorker);
					break;
				case 5:
					return;
				case 6:
					goToReports(loggedWorker);
					break;
				case 0:
					System.exit(1);
			}
	
		}
			
	} //od workerMeni metode
	
	public static void goToReports(Worker loggedWorker) {
		
		
		if (!loggedWorker.isBoss()) {
			System.out.println("Nemate privilegija za pregeld izvestaja");
			return;
		}
		
		
		
		while (true) {
			System.out.println("1. Ukupni prihodi i rashodi\n2. Dostavljaci\n3. Ucestalost porucivanja artikala\n. 4. Zavr�ene porudzbine\n5. Prihodi i rashodi izabranog restorana\n0. Povratak na prethodni meni");
			
			int report = Utility.choice(5);
			
			switch(report) {
				case 1:
					summaryReport();
					break;
				case 2:
					goToDeliverersReport();
					break;
				case 3:
					frequencyOfItems();
				case 4:
					deliveredOrders();
				case 5:
					goToRestaurantReport();
			} //od switcha
			
		} //od whila
		
		
		

		
		
		
	} //od izvestaj metode
	
	
	public static void deliveredOrders() {
		System.out.println("Unesite datum za koje zelite da vidite dostavljene porudzbine.");
		Date startDate = Utility.unesiDatum("Unesite pocetni datum: ");
		Date endDate = Utility.unesiDatum("Unesite krajnji datum: ");
		
		ArrayList<Order> orders = findOrderForItems(startDate, endDate);
		
		CustomerUI.displayOrdersForUser(orders);
		
	}
	
	public static void goToRestaurantReport() {
		
		displayRestaurants();
		
		int idRestoran = Utility.ocitajId(FoodChainStore.restaurants, "Unesite id restorana ciji izve�taj zelite da pogledate: ");
		
		restaurantReport((Restaurant)FoodChainStore.restaurants.get(idRestoran));
		
	}
	
	public static void restaurantReport(Restaurant restaurant) {
		System.out.println("Unesite datume pocetka i kraja izve�taja za izabrani restoran.");
		Date startDate = Utility.unesiDatum("Unesite pocetni datum: ");
		Date endDate = Utility.unesiDatum("Unesite krajnji datum: ");
		
		
		double restaurantsIncome = calculateIncome(restaurant, startDate, endDate);
		
		//double deiveryIncome - ne prikazujemo, dostava pripada celom sistemu ne pojedinacnom restoranu..
		
		double expenditures = calculateExpenditures(restaurant, startDate, endDate);
		
		displayReport(restaurant, restaurantsIncome, expenditures);
		
	}
	
	public static void displayReport(Restaurant restaurant, double restaurantsIncome, double expenditures) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("prihodi: " + restaurantsIncome+"\n");
		sb.append("rashodi (samo zaposleni): " + expenditures+"\n");
		
		System.out.println(sb.toString());
		
	}
	
	
	public static double calculateExpenditures(Restaurant restaurant, Date startDate, Date endDate) {
		
		int days = getDays(startDate, endDate);
		
		return FoodChainStore.workers.entrySet().stream()
				.filter(i -> ((Worker)i.getValue()).getRestaurant() == restaurant.getId())
				.mapToDouble(i -> (((Worker)i.getValue()).getSalary()*days)/30.0).sum();		
		
	}
	
	
	public static double calculateIncome(Restaurant restaurant, Date startDate, Date endDate) {
		
		
		ArrayList<Order> orders = findOrderForItems(startDate, endDate);
		
		double p = orders.stream().map(i-> ((ArrayList<Element>) i.getElements()))
				.flatMap(x -> x.stream())
				.filter(x -> ((Item) x.getItem()).getId() == restaurant.getId())
				.mapToDouble(Element::rate).sum();
				
		return (double) p;
		
	
	}

	
	
	
	public static void displayRestaurants() {
		StringBuilder sb = new StringBuilder();
		
		for (Integer idRestoran : FoodChainStore.restaurants.keySet()) {
			Restaurant r = (Restaurant) FoodChainStore.restaurants.get(idRestoran);
			sb.append("id restorana: " + r.getId() + "|naziv: " + r.getName()+"\n");
		}
		
		System.out.println(sb.toString());
		
	}
	
	public static void frequencyOfItems() {
		System.out.println("Unesite datume za koje zelite da vidite ucestalost porucivanja artikala.");
		Date startDate = Utility.unesiDatum("Unesite pocetni datum: ");
		Date endDate = Utility.unesiDatum("Unesite krajnji datum: ");
		
		ArrayList<Order> orderForItems = findOrderForItems(startDate, endDate);
		
		HashMap<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		//key - id itema
		//value - broj porucivanja
		
		for (Order o : orderForItems) {
			for (Element e : o.getElements()) {
				if (!mapa.containsKey(e.getItem().getId())) mapa.put(e.getItem().getId(), 1);
				else mapa.put(e.getItem().getId(), mapa.get(e.getItem().getId())+1);	
			}
		}
		displayFrequency(mapa);
	}
	
	
	
	
	public static void displayFrequency(HashMap<Integer, Integer> mapa) {
		
		StringBuilder sb = new StringBuilder();
		
		for (Integer idItema : mapa.keySet()) {
			Item i = (Item) FoodChainStore.items.get(idItema);
			sb.append("Artikal: " + i.getDescription() + "|broj porucivanja: " + mapa.get(idItema)+"\n");
		}
		
		System.out.println(sb.toString());
		
		
	}
	
	public static ArrayList<Order> findOrderForItems(Date startDate, Date endDate){
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Delivered 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().after(startDate) 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().before(endDate))
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
	}
	
	public static void goToDeliverersReport() {
		
		System.out.println("Izaberite dostavljac:");
		displayAllDeliverers();	
		int idDeliverer = Utility.ocitajId(FoodChainStore.deliverers, "Izaberite id dostavljaca ciji izve�taj zelite da pregeldate: ");
		
		Deliverer deliverer = (Deliverer) FoodChainStore.deliverers.get(idDeliverer);
		
		deliverersReport(deliverer);
	}
	
	public static void deliverersReport(Deliverer deliverer) {
		
		
		
		while(true) {
			System.out.println("1. Podaci o dostavljacu\n2. Pregleda dostavljenih porudzbina\n0. Povratak na prethodni meni");
			int izbor =  Utility.choice(2);
			switch(izbor) {
				case 1:
					showInfos(deliverer);
					break;
				case 2:
					showOrders(deliverer);
					break;
				case 0:
					return;
			}
		}
		
	}
	
	public static void showOrders(Deliverer deliverer) {
		
		Date startDate = Utility.unesiDatum("Unesite pocetni datum za izve�taj: ");
		Date endDate = Utility.unesiDatum("Unesite krajnji datum za izve�taj: ");
		
		ArrayList<Order> listOrders = findDeliverersOrders(deliverer, startDate, endDate);
		
		CustomerUI.displayOrdersForUser(listOrders);
		
	}
	
	public static ArrayList<Order> findDeliverersOrders(Deliverer deliverer, Date startDate, Date endDate){
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Delivered 
				&& ((Order) i.getValue()).getDeliverer().getId() == deliverer.getId() 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().after(startDate) 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().before(endDate))
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
	}
	
	public static void showInfos(Deliverer deliverer) {
		System.out.println("Podaci o dostavljacu:");
		StringBuilder sb = new StringBuilder();
		sb.append("ID dostavljaca: " + deliverer.getId()+"\n");
		sb.append("Ime: " + deliverer.getName()+"\n");
		sb.append("Prezime: " + deliverer.getSurname()+"\n");
		sb.append("JMBG: " + deliverer.getJMBG()+"\n");
		sb.append("Pol: " + deliverer.getSex().getText()+"\n");
		sb.append("Adresa: " + deliverer.getAddress()+"\n");
		sb.append("Mobilni: " + deliverer.getMobile()+"\n");
		sb.append("Korisnicko ime: " + deliverer.getUsername()+"\n");
		sb.append("Lozinka: " + deliverer.getPassword()+"\n");
		sb.append("Plata: " + deliverer.getSalary()+"\n");
		sb.append("Vozilo: " + deliverer.getVehicle().getText()+"\n");
		System.out.println(sb.toString());
		
	}
	
	
	public static void displayAllDeliverers() {
		StringBuilder sb = new StringBuilder();
		
		for (Integer idDeliverer : FoodChainStore.deliverers.keySet()) {
			Deliverer d = (Deliverer) FoodChainStore.deliverers.get(idDeliverer);
			sb.append("Id dostavljaca: " + d.getId() + "|koriscnicko ime: " + d.getUsername()+"\n");
		}
		System.out.println(sb.toString());
	}
	
	@SuppressWarnings({ "unused", "unlikely-arg-type" })
	public static void summaryReport() {
		
		Date startDate = Utility.unesiDatum("Unesite pocetni datum za izve�taj: ");
		Date endDate = Utility.unesiDatum("Unesite krajnji datum za izve�taj: ");
		
		
		//izolovanje porudzbina iz svih restorana koje su Delivered i nalaze se izmedju start i end data
		ArrayList<Order> orders = isolateOrders(startDate,endDate);
		
		//kreiranje mape koja kao kljuc ima id svakog restorana iz orders-a, value je ArrayLista
		//svih onih elemenata ciji Itemi poticu iz tog restorana
		HashMap<Integer, ArrayList<Element>> mapa = findMap(orders);
		
		//incomes
		HashMap<Integer, Double> incomes = findIncomes(mapa);
		double incomeDeliveries = findIncomeDeliveries(orders);
		
		//Expenditures
		Set<Integer> idRestaurants = incomes.keySet();
		double employeesExpenditure = findEmployeesExpenditure(idRestaurants, startDate, endDate);
		
		
		//prikaz preko sb
		StringBuilder sb = new StringBuilder("Prihodi od restorana:"+"\n");
		
		double totalIncome = 0;
		for (Integer idRestaurant : incomes.keySet()) {
			Restaurant r = (Restaurant)FoodChainStore.restaurants.get(idRestaurants);
			totalIncome += incomes.get(idRestaurants);
			sb.append("Restoran: " + r.getName() + "|prihod: " + incomes.get(idRestaurants)+"\n");
			
		}
		
		sb.append("Prihodi od dostave: " + incomeDeliveries+"\n");
		sb.append("\n");
		sb.append("Rashodi na zaposlene (radinici i dostavljaci): " + employeesExpenditure+"\n");
		sb.append("Total: " + (totalIncome - employeesExpenditure)+"\n");
		
	}
	
	public static double findEmployeesExpenditure(Set<Integer> idRestaurants, Date startDate, Date endDate) {
		ArrayList<Worker> listaWorkera = new ArrayList<Worker>();
		 
		Iterator<Integer> iter = idRestaurants.iterator(); 
		 while(iter.hasNext()) {
			 listaWorkera.addAll(findEmployees((Integer)iter.next()));
		 }
		 
		 
		 int numberOfDays = getDays(startDate, endDate);
		 
		 
		 return calculate(listaWorkera, numberOfDays) + calculateDeliveryExpenditures(numberOfDays);
	}
	
	public static double calculateDeliveryExpenditures(int numberOfDays) {
		double total = 0;
		for (Integer idDeliverer : FoodChainStore.deliverers.keySet()) {
			Deliverer d = (Deliverer) FoodChainStore.deliverers.get(idDeliverer);
			total += d.getSalary();
		}
		return total;
	}
	
	public static double calculate(ArrayList<Worker> listaWorkera, int numberOfDays) {
		double total = 0;
		for (Worker w : listaWorkera) {
			double salary = w.getSalary();
			total += (salary * numberOfDays) / 30.0;	//30.0 plata koja je u fajlu je za 30 dana..
		}
		
		return total;
		
	}
	
	public static int getDays(Date startDate, Date endDate) {
		 long diff = endDate.getTime() - startDate.getTime();
	     return (int) diff / 1000 / 60 / 60 / 24; 
	}
	
	public static ArrayList<Worker> findEmployees(int idRestorana){
		ArrayList<Worker> listaZapolsenih = new ArrayList<Worker>();
		for (Integer idWorker : FoodChainStore.workers.keySet()) {
			if (((Worker) FoodChainStore.workers.get(idWorker)).getRestaurant() == idRestorana) listaZapolsenih.add((Worker) FoodChainStore.workers.get(idWorker));
		}
		
		return listaZapolsenih;
	}
	
	public static double findIncomeDeliveries(ArrayList<Order> orders) {
		double total = 0;
		for (Order o : orders) {
			total += o.getDelivery();
		}
		return total;
	}
	
	
	
	public static HashMap<Integer, Double> findIncomes(HashMap<Integer, ArrayList<Element>> mapa){

		 final HashMap<Integer, Double> m = new HashMap<>();
	     mapa.entrySet().stream().forEach(entry -> m.put(entry.getKey(), Double.valueOf(entry.getValue().stream().mapToDouble(Element::rate).sum())));
	     return m;
		
		
	}
	
	public static HashMap<Integer, ArrayList<Element>> findMap(ArrayList<Order> orders){
		
		HashMap<Integer, ArrayList<Element>> mapa = new HashMap<Integer, ArrayList<Element>>();
		
		for (Order o : orders) {
			for (Element e : o.getElements()) {
				if (e.isBonusElement()) continue;
				Item i = e.getItem();
				int idRestorana = i.getRestaurant();
				if (!mapa.containsKey(idRestorana)) {
					mapa.put(idRestorana, new ArrayList<Element>());
				}
				mapa.get(idRestorana).add(e);
			}
		}
		return mapa;
	}
	
	public static ArrayList<Order> isolateOrders(Date startDate, Date endDate){
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getStatus() == Status.Delivered && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().after(startDate) 
				&& ((Order) i.getValue()).getStates().get(((Order) i.getValue()).
				getStates().size()-1).getDate().before(endDate))
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
		
	}
	
	
	
	public static void authorize(Worker loggedWorker) {
		if (!loggedWorker.isBoss()) {
			System.out.println("Nemate privilegija za autorizaciju.");
			return;
		}
		
		showWorker(loggedWorker);
		
		int idworker = Utility.ocitajId(FoodChainStore.workers, "Unesite id zaposlenoh kojem zelite da prenesete menadzerske privilegije: ");
		
		Worker worker = (Worker) FoodChainStore.workers.get(idworker);
		worker.setBoss(true);;
		System.out.println("Uspe�no prene�ene mendazerske privilegije.");
	}

	public static void showWorker(Worker loggedWorker) {
		FoodChainStore.workers.entrySet().stream().filter(i -> !((Worker)i.getValue()).isBoss() && ((Worker)i.getValue()).getId() != loggedWorker.getId() )
		.forEach(i -> System.out.println(i));
	}

	public static void addNewCustomer(Worker loggedWorker) {
		
		if (!loggedWorker.isBoss()) {
			System.out.println("Nemate privilegija za dodavanje novog klijenta");
			return;
		}
		
		
		Customer newCustomer = new Customer();
		
		newCustomer.setId(FoodChainStore.generateId(FoodChainStore.customers));
		newCustomer.setName(Utility.ocitajTekst("Unesite ime: "));
		newCustomer.setSurname(Utility.ocitajTekst("Unesite prezime: "));
		newCustomer.setJMBG(Utility.ocitajTekst("Unesite JMBG: "));
		newCustomer.setSex(Utility.getSex("Izaberite pol."));
		newCustomer.setAddress(Utility.ocitajTekst("Unesite adresu"));
		newCustomer.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
		newCustomer.setUsername(Utility.inputUsername());
		newCustomer.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
		newCustomer.setRole(Role.Customer);
		newCustomer.setAktivan(true);
		newCustomer.setSpecial(false);
		newCustomer.setCredit(0);
		
		try {
			FoodChainStore.customers.put(newCustomer.getId(), newCustomer);
			FoodChainStore.dodaj(newCustomer);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
	
	public static void addNewWorker(Worker loggedWorker) {
		if (!loggedWorker.isBoss()) {
			System.out.println("Nemate privilegija za dodavanje novog klijenta");
		}
		
		Worker newWorker = new Worker();
		
		newWorker.setId(FoodChainStore.generateId(FoodChainStore.workers));
		newWorker.setName(Utility.ocitajTekst("Unesite ime: "));
		newWorker.setSurname(Utility.ocitajTekst("Unesite prezime: "));
		newWorker.setJMBG(Utility.ocitajTekst("Unesite JMBG: "));
		newWorker.setSex(Utility.getSex("Izaberite pol."));
		newWorker.setAddress(Utility.ocitajTekst("Unesite adresu"));
		newWorker.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
		newWorker.setUsername(Utility.inputUsername());
		newWorker.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
		newWorker.setRole(Role.Customer);
		newWorker.setAktivan(true);
		newWorker.setSalary(Utility.ocitajRealanBroj("Unesite platu: "));
		newWorker.setRestaurant(setRestoran());
		newWorker.setBoss(false);
		
		try {
			FoodChainStore.workers.put(newWorker.getId(), newWorker);
			FoodChainStore.dodaj(newWorker);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	private static int setRestoran() {
		StringBuilder sb = new StringBuilder();
	
		for (Integer idRestorana : FoodChainStore.restaurants.keySet()) {
			Restaurant r = (Restaurant) FoodChainStore.restaurants.get(idRestorana);
			sb.append("id restorana: " + r.getId() + "|" + "restoran: " + r.getName()+"\n");
		}
		System.out.println(sb.toString());
		int restoran = Utility.ocitajId(FoodChainStore.restaurants, "Unesite id restorana kako bi izabarli u kom restoranu ce za posleni raditi:");
		return restoran;
	}
	
	public static void acceptOrders(Worker loggedWorker,ArrayList<Order> sortedOrders) {
		
		
		
		if (sortedOrders == null) {
			System.out.println("Trenutno nema porucenih porudzbina na cekanju.");
			return;
		}
		
		//display orders
		//izaberi jednu
		//stavi status prihvaceno sa vremenom i kada ce biti gotovo...
		
		
		CustomerUI.displayOrdersForUser(sortedOrders);
		int indexOrder = Utility.choiceForElement(sortedOrders.size()-1, "Izaberite redni broj porudzbine koju zelite da prihvatite: ");
		//pogledaj istoriju ovog customera
		//da li zelite da dodate gratis artikal sa kolici
		Order acceptedOrder = sortedOrders.get(indexOrder);
		
		StateProtocol acceptedState = new StateProtocol();
		acceptedState.setId(FoodChainStore.generateId(FoodChainStore.protocols));
		acceptedState.setOrder(acceptedOrder.getId());
		acceptedState.setDate(new Date());
		acceptedState.setStatus(Status.Accepted);
		
		FoodChainStore.protocols.put(acceptedState.getId(), acceptedState);
		
		
		StateProtocol processedState = new StateProtocol();
		processedState.setId(FoodChainStore.generateId(FoodChainStore.protocols));
		processedState.setOrder(acceptedOrder.getId());
		processedState.setDate(inputProcessdDate(acceptedState.getDate()));
		processedState.setStatus(Status.Processed);
		
		FoodChainStore.protocols.put(processedState.getId(), processedState);
		
		acceptedOrder.getStates().add(acceptedState);
		acceptedOrder.getStates().add(processedState);
		
		if (loggedWorker.isBoss()) addBounsElements(acceptedOrder);
			
		try {
			FoodChainStore.izmeni(acceptedOrder);
			FoodChainStore.dodaj(acceptedState);
			FoodChainStore.dodaj(processedState);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		
		
		System.out.println("Uspe�no prihvacena porudzbina.");
		
		System.out.println();
		
		
	}
	
	public static void addBounsElements(Order acceptedOrder) {
		
		while (true) {
			
			System.out.println("1. Pregledaj istoriju dostavljenih porudzbina za ovog klijenta\n2. Dodaj bonus artikle\n0. Prihvati porudzbinu");
			
			int izbor = Utility.choice(2);
			
			switch(izbor) {
				case 1:
					//istorija
					if (acceptedOrder.getUser() instanceof Customer) {
						showHistory((Customer)acceptedOrder.getUser());
					}else {
						System.out.println("Porucilac ove porudzbine je zaposleni u restoranu. Klijent nije registrovan u sistemu.");
					}
					
					break;
				case 2:
					//dodaj free artikle
					addAdditionalElements(acceptedOrder);
					break;
				case 0:
					//potvrdi por
					return;
			}
		}
		
	}
	
	public static void addAdditionalElements(Order acceptedOrder) {
		
		while (true) {
			System.out.println(CustomerUI.showAllRestaurantsOffer());
			
			Element newElement = new Element();
			
			newElement.setId(FoodChainStore.generateId(FoodChainStore.elements));
			
			var idItem = Utility.ocitajId(FoodChainStore.items, "Izaberite id artikla koji zelite da dodate u porudzbinu: " );
			
			newElement.setItem((Item)FoodChainStore.items.get(idItem));
			
			newElement.setAmount(Utility.ocitajCeoBroj("Unesite kolicinu: "));
			
			newElement.setOrder(acceptedOrder.getId());
			
			newElement.setBonusElement(true);
			
			acceptedOrder.getElements().add(newElement); //dodajemo ga u listu elemenata same porudzbine

			CustomerUI.showEditedOrder(acceptedOrder);
			
			if (Utility.ocitajOdlukuOPotvrdi("da dodate novi bonus artikal u porudzbinu") == 'Y') {
				FoodChainStore.elements.put(newElement.getId(), newElement); //dodajemo ga i u global mapu
				acceptedOrder.setRestaurantPickup(CustomerUI.findRestaurnatPickup(acceptedOrder.getElements()));
				try {
					FoodChainStore.dodaj(newElement);
					FoodChainStore.izmeni(acceptedOrder);
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				System.out.println("Bonus artikal uspe�no dodat na porudzbinu");
				
			} else {
				acceptedOrder.getElements().remove(acceptedOrder.getElements().size()-1);
				
			}
			if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju uno�enja novih bonus artikal i potvrdite porudzbinu") == 'Y') {
				return;
			}
			
			
			
		//(
		} //od while
		
		
	}
	
	public static void showHistory(Customer customer) {
		//prikaz istorije customera samo dostavljene porudzbine
		
		ArrayList<Order> customerOrders = findCustomersOrder(customer);
		
		if (customerOrders.size() == 0) {
			System.out.println("Ovaj klijent nema dostavljenih porudzbina");
			return;
		}
		
		CustomerUI.displayOrdersForUser(customerOrders);
		
	}
	
	public static ArrayList<Order> findCustomersOrder(Customer customer){
		
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Delivered && ((Order) i.getValue()).getUser().getId() == customer.getId() )
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
		
		
	}
	
	
	
	
	public static Date inputProcessdDate(Date accpetedDate) {
		while (true) {
			Date processedDate = Utility.unesiDatum("Unesite ocekivano vreme zavrsetka pravljenja porudzbine [dd-mm-yyyy hh:mm]: ");
			if (processedDate.after(accpetedDate) && processedDate.after(new Date())) return processedDate;
			else System.out.println("Vreme zavr�etka mora biti nakon vremena prihvatanja porudzbine i nakon sada�njeg trenutka.");
		}
	}
	
	public static void showDeliverers() {
		for(Integer idDeliverer : FoodChainStore.deliverers.keySet()) {
			if (((Deliverer) FoodChainStore.deliverers.get(idDeliverer)).isAktivan()) {
				System.out.println(((Deliverer)FoodChainStore.deliverers.get(idDeliverer)).toString()+"\n");
			}
		}
		System.out.println();
	}
	
	private static ArrayList<Order> findAndSort(){
		
		ArrayList<Order> unsortedOrders = findOrders();
		
		
		System.out.println("Izaberite redosled prikaza porucenih porudzbina.");
		System.out.println("1. Od najstarije ka najnovijoj\n2. Od najnovije ka najstarijoj");
		ArrayList<Order> sortedOrders = null;
		
		int izbor = Utility.choice(2);
		
		switch (izbor) {
			
			
			case 1:
					
				sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			case 2:
				 sortedOrders =  (ArrayList<Order>) unsortedOrders.stream()
				.map(i -> (ArrayList<StateProtocol>) i.getStates())
				.map(i -> (StateProtocol) i.get(i.size()-1))
				.sorted(new StateProtocolComparator(-1))
				.map(i -> (Order) FoodChainStore.orders.get(i.getOrder()))
				.collect(Collectors.toList());
				 break;
			
			default:
				System.out.println("Unesi sorter!");
				
		}
		
		
		
		return sortedOrders;
	}
	
	
	
	private static ArrayList<Order> findOrders() {
		return new ArrayList<Order>(FoodChainStore.orders.entrySet().stream()
				.filter(i -> ((Order) i.getValue()).isAktivna() && 
				((Order) i.getValue()).getStates().get(((Order) i.getValue()).getStates().size()-1).getStatus() == Status.Ordered)
				.map(i -> ((Order) i.getValue())).collect(Collectors.toList()));
		
			
	}
	
}
