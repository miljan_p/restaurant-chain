package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import DAO.DAOinterface;
import model.Bonus;
import model.Customer;
import model.Deliverer;
import model.Element;
import model.Identifiable;
import model.Item;
import model.Order;
import model.Restaurant;
import model.StateProtocol;
import model.User;
import model.Worker;


public class FoodChainStore {
	
	public static HashMap<Integer, Identifiable> customers = null;
	public static HashMap<Integer, Identifiable> workers = null;
	public static HashMap<Integer, Identifiable> deliverers = null;
	public static HashMap<Integer, Identifiable> items = null;
	public static HashMap<Integer, Identifiable> restaurants = null;
	public static HashMap<Integer, Identifiable> elements = null;
	public static HashMap<Integer, Identifiable> orders = null;
	public static HashMap<Integer, Identifiable> bonuses = null;
	public static HashMap<Integer, Identifiable> protocols = null; 


	public static void initialLoad() {
		
		
		customers = DAOinterface.ucitajSve(Customer::CreateFromString, DAOinterface.customersPath);

		workers = DAOinterface.ucitajSve(Worker::CreateFromString, DAOinterface.workersPath);
		
		deliverers = DAOinterface.ucitajSve(Deliverer::CreateFromString, DAOinterface.deliverersPath);
		
		items = DAOinterface.ucitajSve(Item::CreateFromString, DAOinterface.itemsPath);
	
		restaurants = DAOinterface.ucitajSve(Restaurant::CreateFromString, DAOinterface.restaurantsPath);
		
		elements = DAOinterface.ucitajSve(Element::CreateFromString, DAOinterface.elementsPath);
		
		bonuses = DAOinterface.ucitajSve(Bonus::CreateFromString, DAOinterface.bonusesPath);
		
		protocols = DAOinterface.ucitajSve(StateProtocol::CreateFromString, DAOinterface.protocolsPath);
		
		orders = DAOinterface.ucitajSve(Order::CreateFromString, DAOinterface.ordersPath);


	}
	
	public static HashSet<Worker> ucitajRadnikeRestorana(int restaurantId){
		return new HashSet<Worker>(workers.entrySet().stream().filter(i -> ((Worker) i.getValue()).getRestaurant() == restaurantId)
				.map(i -> ((Worker) i.getValue())).collect(Collectors.toSet()));
	}
	
	
	public static HashSet<Item> ucitajItemsRestorana(int restaurantId){
		return new HashSet<Item>(items.entrySet().stream().filter(i -> ((Item) i.getValue()).getRestaurant() == restaurantId)
				.map(i -> ((Item) i.getValue())).collect(Collectors.toSet()));
	}	
		
	
	public static ArrayList<Element> ucitajElementePorudzbine(int idPorudzbine){
		return new ArrayList<Element>(elements.entrySet().stream().filter(i -> ((Element) i.getValue()).getOrder() == idPorudzbine)
				.map(i -> ((Element) i.getValue())).collect(Collectors.toList()));
	}
	
	public static int generateId(HashMap<Integer, Identifiable> list) {
		if (!list.isEmpty()) {
			return list.values().stream().map(i -> i.getId()).max(Integer::compare).get() + 1;
		} else
			return 0;
	}
	
	public static Bonus ucitajBonusPorudzbine(int idPorudzbine) {
		
		//treba da nadjes jedan bonus za datu porudzbinu...
		
		if (!bonuses.isEmpty()) {
			ArrayList<Bonus> lista =  (ArrayList<Bonus>) bonuses.entrySet().stream()
					.filter(i -> ((Bonus) i.getValue() ).getOrder() == idPorudzbine)
					.map(i->  ((Bonus) i.getValue() )).collect(Collectors.toList());
			if (!lista.isEmpty() && lista.size() == 1) return lista.get(0);
		}
		System.out.println("nije pronadjen bonus");
		return null;
	}
	
	public static ArrayList<StateProtocol> ucitajProtokoleStanjaPorudzbine(int idPorudzbine){
		
		return new ArrayList<StateProtocol>(protocols.entrySet().stream().filter(i -> ((StateProtocol) i.getValue()).getOrder() == idPorudzbine)
				.map(i -> ((StateProtocol) i.getValue())).collect(Collectors.toList()));
		
		
	}
	
	public static Deliverer ucitajDostavljaca(int idDostavljaca) {
		if (idDostavljaca == -1) return null;
		return (Deliverer) deliverers.get(idDostavljaca);
	}
	
	public static User login(String username, String password, User user) {
		
		User u = null;
	
		if (user instanceof Customer) {
			try {
				u = customers.entrySet().stream()
						.filter(e -> ((Customer) e.getValue()).getUsername().equals(username) && 
								((Customer) e.getValue()).getPassword().equals(password))
						.map(e -> ((Customer) e.getValue()))
						.findFirst().get();
				return u;
			} catch (Exception e) {
				System.out.println("Pogre�no une�eni korisnicko ime ili lozinka. Poku�ajte ponovo.");			
			}

		} else if (user instanceof Deliverer) {
			try {
				u = deliverers.entrySet().stream()
						.filter(e -> ((Deliverer) e.getValue()).getUsername().equals(username) && 
								((Deliverer) e.getValue()).getPassword().equals(password))
						.map(e -> ((Deliverer) e.getValue()))
						.findFirst().get();
				return u;
			} catch (Exception e) {
				System.out.println("Pogre�no une�eni korisnicko ime ili lozinka. Poku�ajte ponovo.");		
			}
		
		} else if (user instanceof Worker) {
			try {
				u = workers.entrySet().stream()
						.filter(e -> ((Worker) e.getValue()).getUsername().equals(username) && 
								((Worker) e.getValue()).getPassword().equals(password))
						.map(e -> ((Worker) e.getValue()))
						.findFirst().get();
				return u;
			} catch (Exception e) {
				System.out.println("Pogre�no une�eni korisnicko ime ili lozinka. Poku�ajte ponovo.");
			}
		}
			return null;
		}



	public static void dodaj(Identifiable object) throws Exception {
		
		
			if (object instanceof Customer) {
				DAOinterface.dodaj(object, DAOinterface.customersPath);
			} else if (object instanceof Deliverer) {
				DAOinterface.dodaj(object, DAOinterface.deliverersPath);
			} else if (object instanceof Worker) {
				DAOinterface.dodaj(object, DAOinterface.workersPath);
			} else if (object instanceof Item) {
				DAOinterface.dodaj(object, DAOinterface.itemsPath);
			} else if (object instanceof Restaurant) {
				DAOinterface.dodaj(object, DAOinterface.restaurantsPath);
			} else if (object instanceof Order) {
				if (((Order) object).getBonus() != null) {
					DAOinterface.dodaj(((Order) object).getBonus(), DAOinterface.bonusesPath);
					User user = ((Order) object).getUser();
					if (user instanceof Customer) DAOinterface.izmeni(user, DAOinterface.customersPath);
				}
				((Order) object).getElements().stream().forEach(i -> {
					try {
						DAOinterface.dodaj(i, DAOinterface.elementsPath);					
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
						e.printStackTrace();
					}
				});
				
				try {
					DAOinterface.dodaj(((Order) object).getStates().get(((Order) object).getStates().size() - 1), DAOinterface.protocolsPath);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				}
				DAOinterface.dodaj(object,  DAOinterface.ordersPath);
			} else if (object instanceof Bonus){
				DAOinterface.dodaj(object, DAOinterface.bonusesPath);	
			} else if (object instanceof Element) {
				DAOinterface.dodaj(object, DAOinterface.elementsPath);
			} else if(object instanceof StateProtocol) {
				DAOinterface.dodaj(object, DAOinterface.protocolsPath);
			} else {
				throw new Exception("Object not supported");
			}
		
	}
	
	public static void izmeni(Identifiable object) throws Exception {
		
		
		if (object instanceof Customer) {
			DAOinterface.izmeni(object, DAOinterface.customersPath);
		} else if (object instanceof Deliverer) {
			DAOinterface.izmeni(object, DAOinterface.deliverersPath);
		} else if (object instanceof Worker) {
			DAOinterface.izmeni(object, DAOinterface.workersPath);
		} else if (object instanceof Item) {
			DAOinterface.izmeni(object, DAOinterface.itemsPath);
		} else if (object instanceof Restaurant) {
			DAOinterface.izmeni(object, DAOinterface.restaurantsPath);
		} else if (object instanceof Order) {
			DAOinterface.izmeni(object, DAOinterface.ordersPath);
		} else if (object instanceof Element) {
			DAOinterface.izmeni(object, DAOinterface.elementsPath);
		} else if (object instanceof Bonus) {
			DAOinterface.izmeni(object, DAOinterface.bonusesPath);
		} else if (object instanceof StateProtocol) {
			DAOinterface.izmeni(object, DAOinterface.protocolsPath);
		} else {
			throw new Exception("Object not supported");
			
		}
	
	
	}//od metode
	
	
	public static void obrisi(Identifiable object) throws Exception {
		
		
		if (object instanceof Customer) {
			DAOinterface.obrisi(object, DAOinterface.customersPath);
		} else if (object instanceof Deliverer) {
			DAOinterface.obrisi(object, DAOinterface.deliverersPath);
		} else if (object instanceof Worker) {
			DAOinterface.obrisi(object, DAOinterface.workersPath);
		} else if (object instanceof Item) {
			DAOinterface.obrisi(object, DAOinterface.itemsPath);
		} else if (object instanceof Restaurant) {
			DAOinterface.obrisi(object, DAOinterface.restaurantsPath);
		} else if (object instanceof Order) {
			DAOinterface.obrisi(object, DAOinterface.ordersPath);
		} else if (object instanceof Element) {
			DAOinterface.obrisi(object, DAOinterface.elementsPath);
		} else {
			throw new Exception("Object not supported");
		}
	
	
	}//od metode
	
	
	
	public static boolean checkIfUsernameAvailable(String username) {
		var isGood = true;
		try {

			customers.values().stream().filter(i -> ((Customer) i).getUsername().equals(username)).findAny().get();
			isGood = false;

		} catch (Exception e) {
			isGood = true;
		}
		
		try {

			workers.values().stream().filter(i -> ((Worker) i).getUsername().equals(username)).findAny().get();
			isGood = false;

		} catch (Exception e) {
			if (isGood != false)
				isGood = true;
		}
		try {

			deliverers.values().stream().filter(i -> ((Deliverer) i).getUsername().equals(username)).findAny().get();
			isGood = false;

		} catch (Exception e) {
			if (isGood != false)
				isGood = true;
		}
		return isGood;
	}






} //od klase
