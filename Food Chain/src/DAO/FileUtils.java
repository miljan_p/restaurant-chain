package DAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class FileUtils {
	
	public static BufferedReader getReader(String fileName) throws UnsupportedEncodingException, FileNotFoundException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(getFileForName(fileName)), "UTF8"));
	}
	
	public static BufferedWriter getWriter(String fileName, boolean append) throws UnsupportedEncodingException, FileNotFoundException{
		return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(getFileForName(fileName), append), "UTF8"));
	}
	
	public static File getFileForName(String name) {
		
		String dir = System.getProperty("user.dir");
		String sP = System.getProperty("file.separator");
		File dirPodaci = new File(dir + sP + "src" + sP + "data");
		
		File file = new File(dirPodaci.getAbsolutePath() + sP + name);
		
		return file;
		
	}
	
}
