package DAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import model.Identifiable;

public interface DAOinterface {
	
	public static final String customersPath = "customers.txt";
	public static final String restaurantsPath = "restaurants.txt";
	public static final String itemsPath = "items.txt";
	public static final String workersPath = "workers.txt";
	public static final String deliverersPath = "deliverers.txt";
	public static final String elementsPath = "elements.txt";
	public static final String ordersPath = "orders.txt";
	public static final String bonusesPath = "bonuses.txt";
	public static final String protocolsPath = "protocols.txt";
	public static final String temporaryFilePath = "temporary.txt";

	
	public static HashMap<Integer,Identifiable> ucitajSve(CreateFromStringInterface createFromString, String path) {
		
		var mapa = new HashMap<Integer,Identifiable>();
		
		BufferedReader reader = null;
		
		try {
			reader = FileUtils.getReader(path);
		
			String line = "";
			while((line = reader.readLine()) != null  ) {
				
				if(!line.equals("")) {
					var object = createFromString.CreateFromString(line);
					mapa.put(object.getId(), object);
				} else {
					continue;
				}
			}
		
			reader.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,e.getMessage());
			System.out.println("PERAA");
			e.getStackTrace();
		}
		return mapa;
	}
	
	public static boolean dodaj(Identifiable object, String path) throws IOException{
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(object.WriteToString());
			writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
			return false;
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
				return false;
			}

		}
		return true;

	}
	
	
	public static void izmeni(Identifiable object, String path) throws IOException{

		BufferedReader reader = null;
		BufferedWriter writer = null;
		
		try {
			
			reader = FileUtils.getReader(path);
			writer = FileUtils.getWriter(temporaryFilePath, true);
			
			
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.equals("")) {
					continue;
				}
				if (object.getId() == getIdFromString(line)) {
					writer.write(object.WriteToString());
					writer.newLine();
				} else {
					writer.write(line);
					writer.newLine();
				}
			}
			
			reader.close();
			writer.close();
			
			File original = FileUtils.getFileForName(path);
			File temporary = FileUtils.getFileForName(temporaryFilePath);
			
			original.delete();
			temporary.renameTo(FileUtils.getFileForName(path));
			

			
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		} 
	
	}
	
	public static void obrisi(Identifiable object, String path) {
		

		BufferedReader reader = null;
		BufferedWriter writer = null;

		try {
			reader = FileUtils.getReader(path);
			writer = FileUtils.getWriter(temporaryFilePath, true);
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.equals("")) {
					continue;
				}
				if (object.getId() != getIdFromString(line)) {
					writer.write(line);
					writer.newLine();
				}
			}
			
			
			reader.close();
			writer.close();
			File original = FileUtils.getFileForName(path);
			File temporary = FileUtils.getFileForName(temporaryFilePath);
			
			original.delete();
			temporary.renameTo(FileUtils.getFileForName(path));

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

	}
	
	
	private static int getIdFromString(String text) throws NumberFormatException {
		int id = 0;
		String[] a = text.split("\\|");
		id = Integer.parseInt(a[0]);
		return id;
	}

}
