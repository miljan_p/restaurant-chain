package DAO;

import model.Identifiable;

@FunctionalInterface
public interface CreateFromStringInterface {
	public Identifiable CreateFromString(String line);
}
