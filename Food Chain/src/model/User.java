package model;

import java.util.StringJoiner;

public abstract class User extends Identifiable{
	
	
	protected String name;
	protected String surname;
	protected String JMBG;
	protected Sex sex;
	protected String address;
	protected String mobile;
	protected String username;
	protected String password;
	protected Role role;
	protected boolean aktivan;
	
	
	public User(int id, String name, String surname, String JMBG, Sex sex, String address, String mobile, String username,
			String password, Role role, boolean aktivan) {
		
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.JMBG = JMBG;
		this.sex = sex;
		this.address = address;
		this.mobile = mobile;
		this.username = username;
		this.password = password;
		this.role = role;
		this.aktivan = aktivan;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getJMBG() {
		return JMBG;
	}


	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}


	public Sex getSex() {
		return sex;
	}


	public void setSex(Sex sex) {
		this.sex = sex;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public boolean isAktivan() {
		return aktivan;
	}


	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(this.getId()+"").add(this.getName()).add(this.getSurname());
		line.add(this.getJMBG()).add(this.getSex().toFileString());
		line.add(this.getAddress()).add(this.getMobile());
		line.add(this.getUsername()).add(this.getPassword()).add(this.getRole().toFileString());
		line.add(this.isAktivan()+"");
		return line.toString();
	}
	
	
	
	
	

}
	
	
	
	
	
	

