package model;

import java.util.StringJoiner;

public class Worker extends Employee{
	
	private int restaurant;
	private boolean boss;
	
	public Worker(int id, String name, String surname, String JMBG, Sex sex, String address, String mobile,
			String username, String password, Role role, boolean aktivan, double salary, int restaurant, boolean boss) {
		
		super(id, name, surname, JMBG, sex, address, mobile, username, password, role, aktivan, salary);
		
		this.restaurant = restaurant;
		this.boss = boss;
	}
	
	public Worker() {
		this(-1, "", "", "", Sex.Other, "", "", "", "", Role.Worker, false, -1000, -1, false);
	}

	public int getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(int restaurant) {
		this.restaurant = restaurant;
	}

	public boolean isBoss() {
		return boss;
	}

	public void setBoss(boolean boss) {
		this.boss = boss;
	}

	public static Identifiable CreateFromString(String line) {
		
		var worker = new Worker();
		
		
		String[] workerParts = line.split("\\|");
		
		worker.setId(Integer.valueOf(workerParts[0]));
		worker.setName(workerParts[1]);
		worker.setSurname(workerParts[2]);
		worker.setJMBG(workerParts[3]);
		worker.setSex(Sex.getSex(Integer.valueOf(workerParts[4])));
		worker.setAddress(workerParts[5]);
		worker.setMobile(workerParts[6]);
		worker.setUsername(workerParts[7]);
		worker.setPassword(workerParts[8]);
		worker.setRole(Role.getRole(Integer.valueOf(workerParts[9])));
		worker.setAktivan(Boolean.valueOf(workerParts[10]));
		worker.setSalary(Integer.valueOf(workerParts[11]));
		worker.setRestaurant(Integer.valueOf(workerParts[12]));
		worker.setBoss(Boolean.valueOf(workerParts[13]));
		
		return worker;
		
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(super.WriteToString());
		line.add(this.getRestaurant()+"");
		line.add(this.isBoss()+"");
		return line.toString();
		
	}
	
	@Override
	public String toString() {
		return "id: " + this.getId() + "|" + "korisnicko ime: " + this.getUsername();
		
	}
	
}
