package model;

import java.util.StringJoiner;

public class Item extends Identifiable{
	
	private String description;
	private double price;
	private int restaurant;
	
	public Item(int id, String description, double price, int restaurant) {
		super(id);
		this.description = description;
		this.price = price;
		this.restaurant = restaurant;
	}
	
	public Item() {
		this(-1, "", -1000, -1);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(int restaurant) {
		this.restaurant = restaurant;
	}
	
	public static Identifiable CreateFromString(String line) {
		
		var item = new Item();
		
		String[] itemParts = line.split("\\|");
		
		item.setId(Integer.valueOf(itemParts[0]));
		item.setDescription(itemParts[1]);
		item.setPrice(Integer.valueOf(itemParts[2]));
		item.setRestaurant(Integer.valueOf(itemParts[3]));
		
		return item;
		
		
	}
	
	@Override
	public String toString() {
		return "item: " + this.description + " " + "restoran itema: " + this.restaurant;
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"");
		line.add(this.getDescription()).add(this.getPrice()+"").add(this.getRestaurant()+"");

		return line.toString();
	}
	
}
