package model;

import java.util.HashSet;
import java.util.StringJoiner;

import controller.FoodChainStore;

public class Restaurant extends Identifiable{
	
	private String name;
	private String address;
	private HashSet<Item> items;
	private HashSet<Worker> workers;

	public Restaurant(int id, String name, String address, HashSet<Item> items, HashSet<Worker> workers) {
		super(id);
		this.items = items;
		this.workers = workers;
		this.name = name;
		this.address = address;
	}
	
	public Restaurant() {
		this(-1, "", "", new HashSet<Item>(), new HashSet<Worker>());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public HashSet<Item> getItems() {
		return items;
	}

	public void setItems(HashSet<Item> items) {
		this.items = items;
	}

	public HashSet<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(HashSet<Worker> workers) {
		this.workers = workers;
	}
	
	public String showOffer() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Restoran: " + this.getName() + "\n");
		
		for (Item item : this.items) {
			sb.append("\t" + "id artikla: " + item.getId() + " | artikal: " + item.getDescription() + " | cena: " + item.getPrice()+"\n");
		}
		return sb.toString();
		
		
	}
	
	

	public static Identifiable CreateFromString(String line) {
		
		var restaurant = new Restaurant();
		
		String[] restaurantParts = line.split("\\|");
		
		restaurant.setId(Integer.valueOf(restaurantParts[0]));
		restaurant.setName(restaurantParts[1]);
		restaurant.setAddress(restaurantParts[2]);
		restaurant.setWorkers(FoodChainStore.ucitajRadnikeRestorana(restaurant.getId()));
		restaurant.setItems(FoodChainStore.ucitajItemsRestorana(restaurant.getId()));
		
		
		return restaurant;
		
		
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName()).add(this.getAddress());
		return line.toString();
	}
	
	

}
