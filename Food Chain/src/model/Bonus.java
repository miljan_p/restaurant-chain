package model;

import java.util.StringJoiner;

public class Bonus extends Identifiable {
	
	private int order;
	private int credits;
	private double deduction;
	private int lowerLimitForCredit; //koliko je minimalno potrebno da bude porudzbina da bi se dobio jedan kredit
	private int creditForOrder; //koliko dobija kredita za porudzbinu preko odredjenog iznosa
	private int maxCreditPerOrder; //koliko maksimalno kredita je dozvoljeno da se iskoristi po porudzbini
	private int discountPerCredit; //procenata popusta po jednom kreditu
	
	public Bonus(int id, int order, int credits, double deduction, int lowerLimitForCredit, 
			int creditForOrder, int maxCreditPerOrder, int discountPerCredit) {
		super(id);
		this.order = order;
		this.credits = credits;
		this.deduction = deduction;
		this.lowerLimitForCredit = lowerLimitForCredit;
		this.creditForOrder = creditForOrder;
		this.maxCreditPerOrder = maxCreditPerOrder;
		this.discountPerCredit = discountPerCredit;
	}

	public Bonus() {
		this(-1, -1, -1, -1, -1, -1, -1, -1);
	}
	
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public double getDeduction() {
		return deduction;
	}

	public void setDeduction(double deduction) {
		this.deduction = deduction;
	}

	public int getLowerLimitForCredit() {
		return lowerLimitForCredit;
	}

	public void setLowerLimitForCredit(int lowerLimitForCredit) {
		this.lowerLimitForCredit = lowerLimitForCredit;
	}

	public int getCreditForOrder() {
		return creditForOrder;
	}

	public void setCreditForOrder(int creditForOrder) {
		this.creditForOrder = creditForOrder;
	}

	public int getMaxCreditPerOrder() {
		return maxCreditPerOrder;
	}

	public void setMaxCreditPerOrder(int maxCreditPerOrder) {
		this.maxCreditPerOrder = maxCreditPerOrder;
	}

	public int getDiscountPerCredit() {
		return discountPerCredit;
	}

	public void setDiscountPerCredit(int discountPerCredit) {
		this.discountPerCredit = discountPerCredit;
	}

	public static Identifiable CreateFromString(String text) {
		
		var bonus = new Bonus();
		
		String[] bonusParts = text.trim().split("\\|");
		
		bonus.setId(Integer.valueOf(bonusParts[0]));
		bonus.setOrder(Integer.valueOf(bonusParts[1]));
		bonus.setCredits(Integer.valueOf(bonusParts[2]));
		bonus.setDeduction(Double.valueOf(bonusParts[3]));
		bonus.setLowerLimitForCredit(Integer.valueOf(bonusParts[4]));
		bonus.setCreditForOrder(Integer.valueOf(bonusParts[5]));
		bonus.setMaxCreditPerOrder(Integer.valueOf(bonusParts[6]));
		bonus.setDiscountPerCredit(Integer.valueOf(bonusParts[7]));
		
		return bonus;
		
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(this.getId()+"");
		
		line.add(this.getOrder()+"").add(this.getCredits()+"").add(this.getDeduction()+"").add(this.getLowerLimitForCredit()+"")
		.add(this.getCreditForOrder()+"").add(this.getMaxCreditPerOrder()+"").add(this.getDiscountPerCredit()+"");
		
		return line.toString();
	}

}
