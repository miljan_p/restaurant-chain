package model;

public enum Sex {
	
	Male,
	Female,
	Other;
	
	private String text;
	
	static {
		Male.text = "Male";
		Female.text = "Female";
		
	}

	public String getText() {
		return text;
	}
	
	public static Sex getSex(int ord) {
		switch(ord) {
		case 0: return Sex.Female;
		case 1: return Sex.Male;
		default: return Sex.Other;
		}
	}
	
	public int sexToInteger() {
		if (this.text.equals("Female")) {
			return 0;
		} else if (this.text.equals("Male")){
			return 1;
		}	 else {
			return 0;
		}
	}
	
	public String toFileString() {
		if (this.text.equals("Female")) {
			return "0";
		} else if (this.text.equals("Male")){
			return "1";
		}	 else {
			return "-1";
		}
	}

}
