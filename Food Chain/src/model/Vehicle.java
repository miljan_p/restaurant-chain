package model;

public enum Vehicle {
	
	Van,
	Scooter,
	Car,
	Bicycle,
	Undetermined;
	
	private String text;
	
	static {
		Van.text = "Van";
		Scooter.text = "Scooter";
		Car.text = "Car";
		Bicycle.text = "Bicycle";
		Undetermined.text = "Undetermined";
		
	}

	public String getText() {
		return text;
	}
	
	public static Vehicle getVehicle(int ord) {
		switch(ord) {
		case 0: return Vehicle.Van;
		case 1: return Vehicle.Scooter;
		case 2: return Vehicle.Car;
		case 3: return Vehicle.Bicycle;
		default: return Vehicle.Undetermined;
		}
	}

}
