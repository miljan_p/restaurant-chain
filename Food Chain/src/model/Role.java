package model;

public enum Role {
	Customer,
	Deliverer,
	Worker;
	
	private String text;
	
	static {
		Customer.text = "Customer";
		Deliverer.text = "Deliverer";
		Worker.text = "Worker";
	}

	public String getText() {
		return text;
	}
	
	public static Role getRole(int ord) {
		switch(ord) {
		case 0: return Role.Customer;
		case 1: return Role.Deliverer;
		case 2: return Role.Worker;
		default: return null;
		}
	}
	
	public int roleToInteger() {
		if (this.text.equals("Customer")) {
			return 0;
		} else if (this.text.equals("Deliverer")){
			return 1;
		} else if (this.text.equals("Worker")){
			return 2;
		} else {
			return -1;
		}
	}
	
	public String toFileString() {
		if (this.text.equals("Customer")) {
			return "0";
		} else if (this.text.equals("Deliverer")){
			return "1";
		} else if (this.text.equals("Worker")){
			return "2";
		} else {
			return "-1";
		}
	}
	
	
	
	
}
