package model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.StringJoiner;

import comparators.StateProtocolComparator;
import controller.FoodChainStore;
import utility.Utility;
import view.ApplicationUI;

public class Order extends Identifiable{
	
	private ArrayList<Element> elements;	//svi itemi(hrani i pice)
	private ArrayList<StateProtocol> states;
	private User user;
	private String remarks;
	private boolean aktivna;
	private Restaurant restaurantPickup;
	private Bonus bonus; //objekat koji opisuje da li je i 
						//koliko iskorisceno bonus kredita za ovu porudzbinu
					   //null - ukoliko nije iskorisceno
	private double delivery;
	private Deliverer deliverer;

	public Order(int id, ArrayList<Element> elements, ArrayList<StateProtocol> states, User user, String remarks, boolean aktivna, Restaurant restaurantPickup, Bonus bonus, Deliverer deliverer) {
		super(id);
		this.elements = elements;
		this.states = states;
		this.user = user;
		this.remarks = remarks;
		this.aktivna = aktivna;
		this.restaurantPickup = restaurantPickup;
		this.bonus = bonus;
		this.deliverer = deliverer;
	}
	
	public Order() {
		this(-1, new ArrayList<Element>(), null, null, "", false, null, null, null);
	}

	public ArrayList<Element> getElements() {
		return elements;
	}

	public void setElements(ArrayList<Element> elements) {
		this.elements = elements;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public ArrayList<StateProtocol> getStates() {
		return states;
	}

	public void setStates(ArrayList<StateProtocol> states) {
		this.states = states;
	}

	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public boolean isAktivna() {
		return aktivna;
	}

	public void setAktivna(boolean aktivna) {
		this.aktivna = aktivna;
	}
	
	
	public Restaurant getRestaurantPickup() {
		return restaurantPickup;
	}

	public void setRestaurantPickup(Restaurant restaurantPickup) {
		this.restaurantPickup = restaurantPickup;
	}

	public Bonus getBonus() {
		return bonus;
	}

	public void setBonus(Bonus bonus) {
		this.bonus = bonus;
	}

	public double getDelivery() {
		return delivery;
	}

	public void setDelivery(double delivery) {
		this.delivery = delivery;
	}
	

	public Deliverer getDeliverer() {
		return deliverer;
	}

	public void setDeliverer(Deliverer deliverer) {
		this.deliverer = deliverer;
	}

	public static Identifiable CreateFromString(String text) {
		
		var order = new Order();
		
		String[] orderParts = text.trim().split("\\|");
		
		order.setId(Integer.valueOf(orderParts[0]));
		order.setElements(FoodChainStore.ucitajElementePorudzbine(order.getId()));	
		//order.setStatus(Status.getStatus(Integer.valueOf(orderParts[1])));
		//order.setDate(Utility.createDateWithTime(orderParts[2]));
		order.setUser(orderParts[1].equals("Customer") ? 
				(Customer) FoodChainStore.customers.get(Integer.valueOf(orderParts[2])) : 
				( Worker ) FoodChainStore.workers.get(Integer.valueOf(orderParts[2]))); 
		order.setRemarks(orderParts[3]);
		order.setRestaurantPickup((Restaurant) FoodChainStore.restaurants.get(Integer.valueOf(orderParts[4])));
		order.setAktivna(Boolean.valueOf(orderParts[5]));
		order.setBonus(FoodChainStore.ucitajBonusPorudzbine(order.getId()));
		order.setStates(srediStanja(order.getId()));
		order.setDelivery(Double.valueOf(orderParts[6]));
		order.setDeliverer(FoodChainStore.ucitajDostavljaca(Integer.valueOf(orderParts[7])));
		return order;

		
	}
	
	private static ArrayList<StateProtocol> srediStanja(int idPorudzbine){
		ArrayList<StateProtocol> listaStanja = null;
		listaStanja = FoodChainStore.ucitajProtokoleStanjaPorudzbine(idPorudzbine);
		Collections.sort(listaStanja, new StateProtocolComparator(1));
		return listaStanja;
	}
	
	public double getTotal() {
		return this.elements.stream()
				.filter(i -> i.isBonusElement() == false)
				.mapToDouble(i -> i.getItem().getPrice()*i.getAmount()).sum();
	}
	
	
	public double getCurrentDiscountPrice(int credit) {
		return this.getTotal() - this.elements.stream()
				.filter(i -> i.isBonusElement() == false)
				.mapToDouble(i -> i.getItem().getPrice()*i.getAmount() * ((credit * ApplicationUI.DISCOUNT_PER_CREDIT)/100.0) ).sum();

	}
	
	public Double getDiscountPrice() {
		if (this.bonus != null) return getTotal() - this.elements.stream()
		.filter(i -> i.isBonusElement() == false)
		.mapToDouble(i -> i.getItem().getPrice()*i.getAmount() * ((this.getBonus().getCredits() * this.getBonus().getDiscountPerCredit())/100.0) ).sum(); 
		return null;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"");
		
		
		if (this.getUser() instanceof Customer) {
			line.add("Customer");
		} else {
			line.add("Worker");
		}
		line.add(this.getUser().getId()+"");
		
		line.add(this.getRemarks()).add(this.getRestaurantPickup().getId()+"")
		.add(this.isAktivna()+"").add(this.getDelivery()+"");
		
		if(this.getDeliverer() != null) {
			line.add(this.getDeliverer().getId()+"");
		} else {
			line.add("-1");
		}
		
		return line.toString();
	}
	

	public String toStringCustomerOrder() {

		return "id porudzbine: " + this.getId() + " " + "Korisnicko ime: " + this.getUser().getUsername() + " " + "Datum porucivanja: " + Utility.convertDateWithTimeToString(this.getStates().get(0).getDate()) + " " + "Status: "  + this.getStates().get(0).getStatus().toString() + " " + "Napomena: " + this.getRemarks() + " " + "\n";  
	}
	
}

	
