package model;

public class Deliverer extends Employee{
	
	private Vehicle vehicle;
	private String registrationNumber;
	
	public Deliverer(int id, String name, String surname, String JMBG, Sex sex, String address, String mobile,
			String username, String password, Role role, boolean aktivan, double salary, Vehicle vehicle, String registrationNumber) {
		
		super(id, name, surname, JMBG, sex, address, mobile, username, password, role, aktivan, salary);
		
		this.registrationNumber = registrationNumber;
		this.vehicle = vehicle;
	}
	
	public Deliverer() {
		this(-1, "", "", "", Sex.Other, "", "", "", "", Role.Deliverer, false, -1000, Vehicle.Undetermined, "" );
	}


	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public static Identifiable CreateFromString(String line) {
		
		var deliverer = new Deliverer();
		
		String[] delivererParts = line.split("\\|");
		
		deliverer.setId(Integer.valueOf(delivererParts[0]));
		deliverer.setName(delivererParts[1]);
		deliverer.setSurname(delivererParts[2]);
		deliverer.setJMBG(delivererParts[3]);
		deliverer.setSex(Sex.getSex(Integer.valueOf(delivererParts[4])));
		deliverer.setAddress(delivererParts[5]);
		deliverer.setMobile(delivererParts[6]);
		deliverer.setUsername(delivererParts[7]);
		deliverer.setPassword(delivererParts[8]);
		deliverer.setRole(Role.getRole(Integer.valueOf(delivererParts[9])));
		deliverer.setAktivan(Boolean.valueOf(delivererParts[10]));
		deliverer.setSalary(Integer.valueOf(delivererParts[11]));
		deliverer.setVehicle(Vehicle.getVehicle(Integer.valueOf(delivererParts[12])));
		
		return deliverer;
		
	}
	
	@Override
	public String toString() {
		return "id dostavljaca: " + this.getId() + " " + "korisnicko ime: " + this.getUsername() + "|" + "vozilo: " + this.vehicle.getText();
				
	}
}
