package model;

import java.util.StringJoiner;

public class Customer extends User{
	
	private boolean special;
	private int credit;
	
	public Customer(int id, String name, String surname, String JMBG, Sex sex, String address, String mobile,
			String username, String password, Role role, boolean aktivan, boolean special, int credit) {
		
		super(id, name, surname, JMBG, sex, address, mobile, username, password, role, aktivan);
		
		this.special = special;
		this.credit = credit;
	}
	
	public Customer() {
		this(-1, "", "", "", Sex.Other, "", "", "", "", Role.Customer, false, false, -1);
	}

	
	public boolean isSpecial() {
		return special;
	}

	public void setSpecial(boolean special) {
		this.special = special;
	}


	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public static Identifiable CreateFromString(String text) {
		
		var customer = new Customer();
		
		String[] customerParts = text.trim().split("\\|");
		
		customer.setId(Integer.valueOf(customerParts[0]));
		customer.setName(customerParts[1]);
		customer.setSurname(customerParts[2]);
		customer.setJMBG(customerParts[3]);
		customer.setSex(Sex.getSex(Integer.valueOf(customerParts[4])));
		customer.setAddress(customerParts[5]);
		customer.setMobile(customerParts[6]);
		customer.setUsername(customerParts[7]);
		customer.setPassword(customerParts[8]);
		customer.setRole(Role.getRole(Integer.valueOf(customerParts[9])));
		customer.setAktivan(Boolean.valueOf(customerParts[10]));
		customer.setSpecial(Boolean.valueOf(customerParts[11]));
		customer.setCredit(Integer.valueOf(customerParts[12]));
		
		return customer;
	}
	
	@Override
	public String WriteToString() {
		
		var line = new StringJoiner("|");
		line.add(super.WriteToString());
		line.add(this.isSpecial()+"");
		line.add(this.getCredit()+"");
		
		return line.toString();
	}
	
}
