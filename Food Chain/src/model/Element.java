package model;

import java.util.StringJoiner;

import controller.FoodChainStore;

public class Element extends Identifiable{
	
	private Item item; //hrana ili pice
	private int amount;
	private int order;
	private boolean bonusElement; //podatak koji govori da li dati item treba da se 
								 //uzme u obzir za naplatu ili ne 
							//(npr. ako ga je Mrvica naknadno dodao kao free artikala).
	

	
	public Element(int id, Item item, int amount, int order, boolean bonusElement) {
		super(id);
		this.item = item;
		this.amount = amount;
		this.order = order;
		this.bonusElement = bonusElement;
	}
	
	public Element() {
		this(-1, null, -1, -1, false);
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public boolean isBonusElement() {
		return bonusElement;
	}

	public void setBonusElement(boolean bonusElement) {
		this.bonusElement = bonusElement;
	}

	@Override
	public String toString() {
		return "id: " + this.getId() + "|" + "artikal: " + this.item.getDescription() + "|kolicina: " + this.getAmount();
	}
	
	public static Identifiable CreateFromString(String text) {
		
		var element = new Element();
		
		String[] elementParts = text.trim().split("\\|");
		
		element.setId(Integer.valueOf(elementParts[0]));
		element.setItem((Item) FoodChainStore.items.get(Integer.valueOf(elementParts[1])));	
		element.setAmount(Integer.valueOf(elementParts[2]));
		element.setOrder(Integer.valueOf(elementParts[3]));
		element.setBonusElement(Boolean.valueOf(elementParts[4]));
		return element;
		
	}

	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"");
		line.add(this.getItem().getId()+"").add(this.getAmount()+"").add(this.getOrder()+"")
		.add(this.isBonusElement()+"");

		return line.toString();
	}
	
	public double rate() {
		return this.getItem().getPrice() * this.amount;
	}
	
}
