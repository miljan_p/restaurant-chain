package model;

import java.util.Date;
import java.util.StringJoiner;

import utility.Utility;

public class StateProtocol extends Identifiable{
	
	private int order;
	private Status status;
	private Date date;
	


	public StateProtocol(int id, int order, Status status, Date date) {
		super(id);
		this.order = order;
		this.status = status;
		this.date = date;
	}
	
	
	
	public StateProtocol() {
		this(-1, -1, Status.Undetermined, null);
	}
	

	public int getOrder() {
		return order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
	public static Identifiable CreateFromString(String text) {
		
		var stateProtocol = new StateProtocol();
		
		String[] stateParts = text.trim().split("\\|");
		
		stateProtocol.setId(Integer.valueOf(stateParts[0]));
		stateProtocol.setOrder(Integer.valueOf(stateParts[1]));
		stateProtocol.setStatus(Status.getStatus(Integer.valueOf(stateParts[2])));
		stateProtocol.setDate(Utility.createDateWithTime(stateParts[3]));
		
		return stateProtocol;
		
	}
	

	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"");
		line.add(this.getOrder()+"").add(this.getStatus().toFileString())
		.add(Utility.convertDateWithTimeToString(this.getDate()));
		return line.toString();
	}
}
