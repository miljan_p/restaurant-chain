package model;

public enum Status {
	
	Ordered,
	Accepted,
	Processed,
	Delivering,
	Delivered,
	Canceled,
	Undetermined;

	private String text;


	static {
		Ordered.text = "Ordered";
		Accepted.text = "Accepted";
		Processed.text = "Processed";
		Delivering.text = "Delivering";
		Delivered.text = "Delivered";
		Undetermined.text = "Undetermined";
		
	}

	public String toString() {
		return text;
	}

	
	
	public static Status getStatus(int ord) {
		switch(ord) {
		case 0: return Status.Ordered;	//0
		case 1: return Status.Accepted;	//1
		case 2: return Status.Processed;//2
		case 3: return Status.Delivering;//3
		case 4: return Status.Delivered;//4
		case 5: return Status.Canceled;//5
		default: return Status.Undetermined;//-1
		
		}
	}
	
	public String toFileString() {
		if (this.text.equals("Ordered")) {
			return "0";
		} else if (this.text.equals("Accepted")){
			return "1";
		}else if(this.text.equals("Processed")){
			return "2";
		} else if(this.text.equals("Delivering")){
			return "3";
		}else if(this.text.equals("Delivered")){
			return "4";
		} else if(this.text.equals("Canceled")){
			return "5";
		} else {
			return "-1";
		}
			
	}

}
