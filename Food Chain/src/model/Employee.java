package model;

import java.util.StringJoiner;

public abstract class Employee extends User{
	
	private double salary;
	
	public Employee(int id, String name, String surname, String JMBG, Sex sex, String address, String mobile, String username,
			String password, Role role, boolean aktivan, double salary) {
		super(id,name, surname, JMBG, sex, address, mobile, username, password, role, aktivan);
		this.salary = salary;
		
	}
	
	public Employee() {
		this(-1, "", "", "", Sex.Other, "", "", "", "", Role.Customer, false, -1000);
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(super.WriteToString());
		line.add(this.getSalary()+"");
		return line.toString();
	}
	
	

}
